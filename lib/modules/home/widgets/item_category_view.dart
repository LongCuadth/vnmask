import 'package:app/data/models/category/category_model.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class ItemCategoryView extends StatelessWidget {
  const ItemCategoryView({super.key, required this.model});
  final CategoryModel model;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      CachedNetworkImage(
        imageUrl: model.url,
        width: double.maxFinite,
        imageBuilder: (context, imageProvider) => Container(
          width: 65.0,
          height: 65.0,
          margin: const EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            shape: BoxShape.rectangle,
            image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
          ),
        ),
      ).marginOnly(bottom: 2.h),
      Flexible(
          child: Text(
        model.title,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: ThemeProvider.fontSize12 + 1,
            fontWeight: FontWeight.w500),
      ).marginSymmetric(horizontal: 5.w))
    ]);
  }
}
