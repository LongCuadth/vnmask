import 'package:app/base/widgets/base_button.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class ItemVideo extends StatelessWidget {
  const ItemVideo({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(right: 10.w),
          decoration: BoxDecoration(
              border: Border.all(color: ThemeProvider.colorPrimary, width: 0.5),
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(8), topRight: Radius.circular(8))),
          width: 130,
          child: Column(
            children: [
              const Text(
                'Khẩu trang UNICARE 6D PRO',
                textAlign: TextAlign.center,
              ).marginAll(6),
              Stack(
                alignment: Alignment.center,
                children: [
                  CachedNetworkImage(
                    imageUrl:
                        'https://content.pancake.vn/1/s450x450/fwebp/f0/c5/57/d9/760aa28248ccaf8eab3cffde57232bb477260763b84141bdb76561fb.jpeg',
                    width: double.maxFinite,
                    imageBuilder: (context, imageProvider) => Container(
                      height: 125.0,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                        shape: BoxShape.rectangle,
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  CircleAvatar(
                    radius: 25,
                    backgroundColor: Colors.black.withOpacity(0.55),
                    child: const Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 30,
                    ),
                  )
                ],
              )
            ],
          ),
        ),
        BaseButton(
                height: 20,
                radius: 8,
                width: 130,
                title: 'detail'.tr,
                onPressed: () {},
                styleButton: BaseButtonStyle.fill)
            .marginOnly(right: 10.w, top: 10.h)
      ],
    );
  }
}
