import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum SelectShoppingType { retailer, traders }

class ItemSelectType extends StatelessWidget {
  const ItemSelectType(
      {super.key, required this.isSelected, required this.title, this.width});
  final bool isSelected;
  final String title;
  final double? width;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      width: width ?? 150,
      decoration: BoxDecoration(
          color: isSelected ? Colors.red.shade200 : Colors.grey.shade200,
          borderRadius: BorderRadius.circular(40),
          border: isSelected ? Border.all(color: Colors.red) : null),
      child: Center(
        child: Text(title,
            style: TextStyle(
                color: isSelected ? Colors.red : Colors.black,
                fontWeight: FontWeight.w700)),
      ),
    );
  }
}

class ItemPayType extends StatelessWidget {
  const ItemPayType(
      {super.key, required this.isSelected, required this.title, this.width});
  final bool isSelected;
  final String title;
  final double? width;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      width: width ?? 150,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          border: isSelected
              ? Border.all(color: ThemeProvider.colorPrimary)
              : Border.all(color: Colors.grey)),
      child: Center(
        child: Text(title,
            style: TextStyle(
                color: isSelected ? ThemeProvider.colorPrimary : Colors.black,
                fontWeight: FontWeight.w700)),
      ),
    );
  }
}

class ItemHostType extends StatelessWidget {
  const ItemHostType(
      {super.key,
      required this.isSelected,
      required this.sale,
      this.width,
      required this.image});
  final bool isSelected;
  final String sale;
  final double? width;
  final Widget image;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Text('-$sale%',
          style: TextStyle(
              color: isSelected ? Colors.red : Colors.black,
              fontWeight: FontWeight.w700)),
      Container(
        height: 30,
        width: width ?? 150,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            border: isSelected
                ? Border.all(color: ThemeProvider.colorPrimary)
                : Border.all(color: Colors.grey)),
        child: Center(child: image),
      ),
    ]);
  }
}

class ItemMoneyType extends StatelessWidget {
  const ItemMoneyType(
      {super.key,
      required this.isSelected,
      required this.sale,
      required this.price,
      this.width});
  final bool isSelected;
  final String price;
  final String sale;
  final double? width;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      width: width ?? 150,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          border: isSelected
              ? Border.all(color: ThemeProvider.colorPrimary)
              : Border.all(color: Colors.grey)),
      child: Column(
        children: [
          Text(price,
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: ThemeProvider.fontSize12)),
          Text('Giá $sale',
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: ThemeProvider.fontSize12)),
        ],
      ),
    );
  }
}

class ItemStatusType extends StatelessWidget {
  const ItemStatusType({super.key, required this.title, required this.icon});
  final String title;
  final Widget icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        children: [
          icon.marginOnly(bottom: 5),
          Text(title,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                color: Colors.black,
                fontSize: ThemeProvider.fontSize12,
              ))
        ],
      ),
    );
  }
}

class ItemSelectTransactionType extends StatelessWidget {
  const ItemSelectTransactionType(
      {super.key, required this.isSelected, required this.title, this.width});
  final bool isSelected;
  final String title;
  final double? width;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 20,
      width: width ?? 75,
      decoration: BoxDecoration(
          color: isSelected ? Colors.white : Colors.transparent,
          borderRadius: BorderRadius.circular(40),
          border: isSelected ? Border.all(color: Colors.red) : null),
      child: Center(
        child: Text(title,
            style: TextStyle(
                fontSize: ThemeProvider.fontSize12 + 1,
                color: isSelected ? Colors.red : Colors.white,
                fontWeight: FontWeight.w700)),
      ),
    );
  }
}
