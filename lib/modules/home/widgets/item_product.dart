import 'package:app/base/widgets/base_button.dart';
import 'package:app/modules/home/widgets/add_to_cart_widget.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class ItemProduct extends StatelessWidget {
  const ItemProduct({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Colors.white60,
      ),
      child: Column(
        children: [
          CachedNetworkImage(
            imageUrl:
                'https://content.pancake.vn/1/s750x750/fwebp/f0/d8/39/5c/cf01e5b50bef662fda41584a977bc02cfa5a667c13b3215bef229b2f.jpg',
            width: double.maxFinite,
            imageBuilder: (context, imageProvider) => Container(
              width: double.infinity,
              height: 150,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(16),
                    topRight: Radius.circular(16)),
                shape: BoxShape.rectangle,
                image: DecorationImage(image: imageProvider, fit: BoxFit.fill),
              ),
            ),
          ).marginOnly(bottom: 5.h),
          Text(
            'Khẩu trang UNICARE chống bụi mịn và kháng khuẩn 99+',
            style: TextStyle(fontSize: ThemeProvider.fontSize12 + 1),
          ).marginOnly(left: 4, right: 4, top: 5.h, bottom: 5.h),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                '79.000đ',
                style: TextStyle(color: Colors.red),
              ).marginOnly(
                left: 4,
                right: 4,
              ),
              Text(
                'Đã bán 10,2k',
                style: TextStyle(
                    color: Colors.black, fontSize: ThemeProvider.fontSize10),
              ).marginOnly(
                left: 4,
                right: 4,
              ),
            ],
          ).marginOnly(bottom: 15.h),
          BaseButton(
              width: 160.w,
              height: 30.h,
              radius: 8,
              title: 'add_to_cart'.tr,
              onPressed: () {
                _showBottomSheet();
              },
              styleButton: BaseButtonStyle.fill)
        ],
      ),
    );
  }

  void _showBottomSheet() {
    Get.bottomSheet(const AddToCartWidget(),
        isDismissible: false, isScrollControlled: true);
  }
}
