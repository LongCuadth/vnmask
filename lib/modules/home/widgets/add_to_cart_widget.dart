import 'package:app/base/widgets/base_button.dart';
import 'package:app/modules/home/controllers/home_controller.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class AddToCartWidget extends GetWidget<HomeController> {
  const AddToCartWidget({super.key, this.isBuyNow = false});
  final bool isBuyNow;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height * 0.7,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
          border: Border.all(color: ThemeProvider.colorPrimary)),
      child: Stack(
        children: [
          _buildSelection().paddingOnly(bottom: 30.h),
          Positioned(
              right: 10,
              top: 5,
              child: GestureDetector(
                  onTap: () {
                    Get.back();
                    controller.numberCount.value = 1;
                    controller.productTypeSelection.value = '';
                  },
                  child: const Icon(Icons.close))),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              padding: EdgeInsets.only(bottom: 20.h, left: 10, right: 10),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'number'.tr,
                        style: const TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),
                      ).marginAll(10),
                      IntrinsicHeight(
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                if (controller.numberCount.value <= 0) return;
                                controller.numberCount.value -= 1;
                              },
                              child: Container(
                                padding: const EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: ThemeProvider.colorPrimary)),
                                child: const Icon(
                                  Icons.remove,
                                  size: 12,
                                ),
                              ),
                            ),
                            Container(
                              width: 40,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: ThemeProvider.colorPrimary)),
                              child: Center(
                                child: Obx(() => Text(
                                    controller.numberCount.value.toString())),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                controller.numberCount.value += 1;
                              },
                              child: Container(
                                padding: const EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: ThemeProvider.colorPrimary)),
                                child: const Icon(
                                  Icons.add,
                                  size: 12,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ).marginOnly(bottom: 10.h),
                  BaseButton(
                      height: 40.h,
                      radius: 8,
                      title: isBuyNow ? 'buy_now'.tr : 'add_to_cart'.tr,
                      textStyle: const TextStyle(
                          fontWeight: FontWeight.w700, color: Colors.white),
                      onPressed: () {
                        Get.back();
                        controller.numberCount.value = 1;
                        controller.productTypeSelection.value = '';
                      },
                      styleButton: BaseButtonStyle.fill),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildSelection() {
    return SingleChildScrollView(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
          CachedNetworkImage(
            imageUrl:
                'https://content.pancake.vn/1/s750x750/fwebp/f0/d8/39/5c/cf01e5b50bef662fda41584a977bc02cfa5a667c13b3215bef229b2f.jpg',
            imageBuilder: (context, imageProvider) => Container(
              width: 120,
              height: 120,
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                image: DecorationImage(image: imageProvider, fit: BoxFit.fill),
              ),
            ),
          ),
          SizedBox(width: 20.w),
          const Text(
            '79.000đ - 150.00đ',
            style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
          )
        ]).marginAll(10),
        const Divider(
          thickness: 0.5,
        ),
        Text(
          'color'.tr,
          style:
              const TextStyle(color: Colors.black, fontWeight: FontWeight.w500),
        ).marginAll(10),
        Obx(
          () => Wrap(
            children: controller.chipChoices.map((choice) {
              return GestureDetector(
                onTap: () {
                  controller.productTypeSelection.value = choice;
                },
                child: Container(
                  padding: const EdgeInsets.all(8),
                  margin: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Colors.grey.shade200,
                      border: controller.productTypeSelection.value == choice
                          ? Border.all(color: ThemeProvider.colorPrimary)
                          : null),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      CachedNetworkImage(
                        imageUrl:
                            'https://content.pancake.vn/1/s750x750/fwebp/f0/d8/39/5c/cf01e5b50bef662fda41584a977bc02cfa5a667c13b3215bef229b2f.jpg',
                        imageBuilder: (context, imageProvider) => Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            image: DecorationImage(
                                image: imageProvider, fit: BoxFit.fill),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10.w,
                      ),
                      Text(
                        choice,
                        style:
                            TextStyle(fontSize: ThemeProvider.fontSize12 + 1),
                      ),
                    ],
                  ),
                ),
              );
            }).toList(),
          ),
        ),
        const SizedBox(
          height: 60,
        )
      ]),
    );
  }
}
