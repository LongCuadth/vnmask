import 'package:app/data/models/category/category_model.dart';
import 'package:app/modules/home/widgets/item_select_type.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  final count = 0.obs;
  var shoppingType = SelectShoppingType.retailer.obs;

  var listCategory = <CategoryModel>[
    CategoryModel(
        title: 'Khẩu trang 5D Mask',
        url:
            'https://content.pancake.vn/1/s400x400/fwebp/2a/02/7d/0d/99341873cb54f0312b16b1947c1a5725975af17927dda3a59bef1faa.jpg'),
    CategoryModel(
        title: 'Khẩu trang 6D Mask',
        url:
            'https://content.pancake.vn/1/s400x400/fwebp/2a/02/7d/0d/99341873cb54f0312b16b1947c1a5725975af17927dda3a59bef1faa.jpg'),
    CategoryModel(
        title: 'Khẩu trang KF94',
        url:
            'https://content.pancake.vn/1/s400x400/fwebp/2a/02/7d/0d/99341873cb54f0312b16b1947c1a5725975af17927dda3a59bef1faa.jpg'),
    CategoryModel(
        title: 'Găng tay cao su y tế',
        url:
            'https://content.pancake.vn/1/s400x400/fwebp/2a/02/7d/0d/99341873cb54f0312b16b1947c1a5725975af17927dda3a59bef1faa.jpg'),
    CategoryModel(
        title: 'Khẩu trang Careion Mask',
        url:
            'https://content.pancake.vn/1/s400x400/fwebp/2a/02/7d/0d/99341873cb54f0312b16b1947c1a5725975af17927dda3a59bef1faa.jpg'),
    CategoryModel(
        title: 'Khẩu trang 6D Anmask Pro',
        url:
            'https://content.pancake.vn/1/s400x400/fwebp/2a/02/7d/0d/99341873cb54f0312b16b1947c1a5725975af17927dda3a59bef1faa.jpg'),
    CategoryModel(
        title: 'Khẩu trang KF94',
        url:
            'https://content.pancake.vn/1/s400x400/fwebp/2a/02/7d/0d/99341873cb54f0312b16b1947c1a5725975af17927dda3a59bef1faa.jpg'),
    CategoryModel(
        title: 'Khẩu trang KF94',
        url:
            'https://content.pancake.vn/1/s400x400/fwebp/2a/02/7d/0d/99341873cb54f0312b16b1947c1a5725975af17927dda3a59bef1faa.jpg'),
  ];

  var categoryScrollController = ScrollController();

  var chipChoices = <String>[
    '50 cái trắng sữa',
    '100 cái trắng sữa',
    '50 cái màu be',
    '100 cái màu be',
    '50 cái màu đen',
    '100 cái màu đen',
    '50 cái màu hồng',
    '100 cái màu hồng',
  ];
  var productTypeSelection = ''.obs;
  var numberCount = 1.obs;
}
