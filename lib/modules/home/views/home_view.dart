import 'package:app/gen/assets.gen.dart';
import 'package:app/modules/home/widgets/item_category_view.dart';
import 'package:app/modules/home/widgets/item_product.dart';
import 'package:app/modules/home/widgets/item_select_type.dart';
import 'package:app/modules/home/widgets/item_video.dart';
import 'package:app/routes/app_pages.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        toolbarHeight: 0,
        elevation: 1,
      ),
      body: SingleChildScrollView(
        child: Stack(children: [
          SizedBox(
            height: Get.height,
            width: Get.width,
            child: Stack(
              children: [
                Assets.image.homeHeader.image(
                    width: double.infinity,
                    fit: BoxFit.fitWidth,
                    height: 320.h),
                _buildHeader(),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 260.h),
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))),
            child: Column(
              children: [
                buildCategory(),
                Container(height: 10, color: Colors.grey.shade400),
                buildListVideo(),
                Container(
                  height: 30,
                  color: Colors.grey.shade200,
                  child: Center(
                    child: Text(
                      'all_product'.tr,
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Container(
                  color: Colors.grey.shade100,
                  child: GridView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: 10,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              childAspectRatio: 0.7,
                              mainAxisSpacing: 10,
                              crossAxisSpacing: 10),
                      itemBuilder: (context, index) {
                        return GestureDetector(
                            onTap: () {
                              Get.toNamed(Routes.DETAIL_PRODUCT);
                            },
                            child: const ItemProduct());
                      }).paddingAll(4),
                ),
                SizedBox(
                  height: 50.h,
                )
              ],
            ),
          )
        ]),
      ),
    );
  }

  Widget _buildHeader() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              flex: 3,
              child: Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                padding: const EdgeInsets.all(2),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(4)),
                child: Row(children: [
                  Assets.image.shield.image(height: 28),
                  Text(
                    'center'.tr,
                    style: TextStyle(
                        fontSize: ThemeProvider.fontSize12,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  )
                ]),
              ),
            ),
            SizedBox(width: 30.w),
            Expanded(
                child: Row(children: [
              GestureDetector(
                onTap: () {
                  Get.toNamed(Routes.NOTIFICATION);
                },
                child: const CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(
                    Icons.notifications,
                    color: Colors.white,
                  ),
                ).marginOnly(right: 10),
              ),
              Assets.image.zalo.image(height: 34),
            ]))
          ],
        ).marginOnly(bottom: 20),
        Align(
          alignment: Alignment.centerRight,
          child: Container(
            width: 180,
            margin: const EdgeInsets.symmetric(vertical: 10),
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
            child: Row(children: [
              Assets.image.logo.image(height: 50).marginOnly(right: 12.w),
              const Text(
                'Hi, UserName',
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
              )
            ]),
          ),
        ),
      ],
    );
  }

  Widget buildCategory() {
    return Container(
      height: 270,
      padding: const EdgeInsets.only(top: 10, bottom: 10),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Obx(
            () => Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  onTap: () {
                    if (controller.shoppingType.value !=
                        SelectShoppingType.retailer) {
                      controller.shoppingType.value =
                          SelectShoppingType.retailer;
                    }
                  },
                  child: ItemSelectType(
                    title: 'retailer'.tr,
                    isSelected: controller.shoppingType.value ==
                        SelectShoppingType.retailer,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    if (controller.shoppingType.value !=
                        SelectShoppingType.traders) {
                      controller.shoppingType.value =
                          SelectShoppingType.traders;
                    }
                  },
                  child: ItemSelectType(
                      title: 'traders'.tr,
                      isSelected: controller.shoppingType.value ==
                          SelectShoppingType.traders),
                ),
              ],
            ),
          ).marginOnly(bottom: 20),
          Expanded(
              child: GridView.builder(
                  controller: controller.categoryScrollController,
                  physics: const BouncingScrollPhysics(),
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: controller.listCategory.length,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      mainAxisSpacing: 10,
                      crossAxisCount: 2,
                      crossAxisSpacing: 10),
                  itemBuilder: (context, index) {
                    return ItemCategoryView(
                        model: controller.listCategory[index]);
                  })),
        ],
      ),
    );
  }

  Widget buildListVideo() {
    return Container(
      height: 250,
      padding: EdgeInsets.only(top: 10.h, left: 10.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'video'.tr,
            style: const TextStyle(
                color: ThemeProvider.colorPrimary, fontWeight: FontWeight.w500),
          ),
          SizedBox(height: 15.h),
          Expanded(
              child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: 10,
            itemBuilder: (BuildContext context, int index) {
              return const ItemVideo();
            },
          ))
        ],
      ),
    );
  }
}
