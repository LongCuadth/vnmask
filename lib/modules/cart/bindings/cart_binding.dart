import 'package:app/modules/cart/controllers/cart_retailer_controller.dart';
import 'package:app/modules/cart/controllers/cart_trader_controller.dart';
import 'package:get/get.dart';

import '../controllers/cart_controller.dart';

class CartBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CartController>(
      () => CartController(),
    );
    Get.lazyPut<CartRetailerController>(
      () => CartRetailerController(),
    );
    Get.lazyPut<CartTraderController>(
      () => CartTraderController(),
    );
  }
}
