import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CartController extends GetxController
    with GetSingleTickerProviderStateMixin {
  late TabController tabarController;
  @override
  void onInit() {
    tabarController = TabController(length: 2, vsync: this);
    super.onInit();
  }
}
