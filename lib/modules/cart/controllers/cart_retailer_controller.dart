import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CartRetailerController extends GetxController {
  var index = 1.obs;
  var totalProduct = 302000.obs;
  var productValue = 302000;
  var shippingValue = 25000;
  var voucher = 10000;
  var totalMoney = 327000.obs;
  var isSelectVoucher = false.obs;
  var isSelectPaymentCOD = true.obs;

  var nameController = TextEditingController();
  var phoneController = TextEditingController();
  var addressController = TextEditingController();
  var addressHomeController = TextEditingController();

  calculateMoney() {
    totalProduct.value = index.value * productValue;
    totalMoney.value = totalProduct.value +
        shippingValue -
        (isSelectVoucher.value ? voucher : 0);
  }
}
