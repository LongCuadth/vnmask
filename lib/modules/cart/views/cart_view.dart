import 'package:app/modules/cart/views/cart_retailer.dart';
import 'package:app/modules/cart/views/cart_trader.dart';
import 'package:app/modules/main_view/controllers/main_view_controller.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/cart_controller.dart';

class CartView extends GetView<CartController> {
  const CartView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('cart'.tr),
          centerTitle: true,
          actions: [
            Center(
                child: GestureDetector(
                    onTap: () {
                      Get.find<MainViewController>().jumpToPage(3);
                    },
                    child: Text('history'.tr).marginOnly(right: 10)))
          ],
        ),
        body: Column(
          children: [
            TabBar(controller: controller.tabarController, tabs: [
              const Tab(
                child: Text(
                  'Giỏ Hàng Mua Lẻ',
                  style: TextStyle(color: ThemeProvider.colorPrimary),
                ),
              ),
              Tab(
                child: Text('Giỏ Hàng Mua Sỉ',
                    style: TextStyle(color: ThemeProvider.colorSecondary)),
              ),
            ]),
            Expanded(
                child: TabBarView(
              controller: controller.tabarController,
              physics: const NeverScrollableScrollPhysics(),
              children: [CartRetailer(), CartTrader()],
            ))
          ],
        ));
  }
}
