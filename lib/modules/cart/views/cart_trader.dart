import 'package:app/base/widgets/base_button.dart';
import 'package:app/gen/assets.gen.dart';
import 'package:app/modules/cart/controllers/cart_trader_controller.dart';
import 'package:app/modules/detail_order/widget/item_product_cart.dart';
import 'package:app/modules/list_order/views/list_order_view.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/format_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

class CartTrader extends GetView<CartTraderController> {
  const CartTrader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  formatDate(DateTime.now()),
                  style: TextStyle(fontSize: ThemeProvider.fontSize12),
                ),
                Text(
                  ' Mã giao dịch: 9521',
                  style: TextStyle(fontSize: ThemeProvider.fontSize12),
                ),
              ],
            ),
            _buildListItem(),
            Divider(
              thickness: 2,
              color: Colors.grey.shade200,
            ),
            _buildInfoPayMent(),
            SizedBox(
              height: 40.h,
            ),
            Obx(
              () => Visibility(
                visible: controller.totalMoney.value < 2000000,
                child: Text(
                  'required_order'.tr,
                  style: TextStyle(
                      color: ThemeProvider.colorPrimary,
                      fontSize: ThemeProvider.fontSize14),
                ).marginOnly(bottom: 5.h),
              ),
            ),
            Obx(
              () => BaseButton(
                      isDisable: controller.totalMoney.value < 2000000,
                      radius: 20,
                      title: 'confirm_order'.tr,
                      onPressed: () {},
                      textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: ThemeProvider.fontSize16),
                      styleButton: BaseButtonStyle.fill)
                  .marginSymmetric(horizontal: 50.w),
            ),
            SizedBox(
              height: 60.h,
            ),
          ],
        ));
  }

  Widget _buildListItem() {
    return Obx(
      () => Visibility(
        visible: controller.index > 0,
        child: ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: 1,
            itemBuilder: (context, index) {
              return Obx(
                () => ItemProductCart(
                  isTrader: true,
                  indexProduct: controller.index.value,
                  onChangeIndex: (index) {
                    if (index == 0) return;
                    controller.index.value = index;
                    controller.calculateMoney();
                  },
                ),
              );
            }),
      ),
    );
  }

  Widget _buildInfoPayMent() {
    return Column(children: [
      Container(
        height: 40,
        decoration: BoxDecoration(
          color: Colors.grey.shade200,
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(80), topRight: Radius.circular(80)),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        margin: const EdgeInsets.symmetric(horizontal: 20),
        child: Row(children: [
          ClipRRect(
              borderRadius: BorderRadius.circular(80),
              child: Assets.image.logo.image()),
          RichText(
            text: TextSpan(
                text: ''.tr,
                style: TextStyle(
                    color: ThemeProvider.colorTextBlack,
                    fontSize: ThemeProvider.fontSize12),
                children: [
                  TextSpan(
                    text: ' VN',
                    style: TextStyle(
                        color: Colors.red,
                        fontWeight: FontWeight.w500,
                        fontSize: ThemeProvider.fontSize12),
                  ),
                  TextSpan(
                    text: 'MASK',
                    style: TextStyle(
                        color: const Color(0xff3171b9),
                        fontWeight: FontWeight.w500,
                        fontSize: ThemeProvider.fontSize12),
                  ),
                  TextSpan(
                    text: ' Voucher',
                    style: TextStyle(
                        color: ThemeProvider.colorTextBlack,
                        fontSize: ThemeProvider.fontSize12),
                  )
                ]),
          ),
          const Spacer(),
          GestureDetector(
            onTap: () {
              controller.isSelectVoucher.value =
                  !controller.isSelectVoucher.value;
              controller.calculateMoney();
            },
            child: Obx(
              () => controller.isSelectVoucher.value
                  ? Container(
                      padding: const EdgeInsets.all(4),
                      margin: EdgeInsets.only(right: 6.w),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          border: Border.all(color: Colors.green)),
                      child: Text(
                        'voucher_10k'.tr,
                        style: TextStyle(
                            color: Colors.green,
                            fontSize: ThemeProvider.fontSize12),
                      ),
                    )
                  : Text(
                      'select_voucher'.tr,
                      style: TextStyle(
                          color: Colors.blue,
                          fontSize: ThemeProvider.fontSize12),
                    ),
            ),
          )
        ]),
      ),
      _buildMoneyInfo(),
      _buildCustomerInfo()
    ]);
  }

  Widget _buildMoneyInfo() {
    return Container(
      margin: EdgeInsets.only(top: 4.h),
      width: double.maxFinite,
      padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 20.h),
      decoration: BoxDecoration(
        color: Colors.grey.shade200,
        borderRadius: const BorderRadius.all(Radius.circular(20)),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('total_product'.tr),
              Obx(() => Text(currencyFormat(controller.totalProduct.value))),
            ],
          ).marginOnly(bottom: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('shipping_pay'.tr),
              Text(currencyFormat(controller.shippingValue)),
            ],
          ).marginOnly(bottom: 5),
          Obx(
            () => Visibility(
              visible: controller.isSelectVoucher.value,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('voucher'.tr),
                  Text(
                    '-${currencyFormat(controller.voucher)}',
                  ),
                ],
              ).marginOnly(bottom: 5),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('total_money'.tr),
              Obx(
                () => Text(
                  currencyFormat(controller.totalMoney.value),
                  style: const TextStyle(
                      color: ThemeProvider.colorPrimary,
                      fontWeight: FontWeight.w700),
                ),
              ),
            ],
          ).marginOnly(bottom: 5),
          Align(
              alignment: Alignment.topLeft,
              child: Text(
                'select_method_payment'.tr,
                style: TextStyle(fontSize: ThemeProvider.fontSize12),
              )).marginOnly(bottom: 5),
          SizedBox(
            height: 20,
            child: Row(
              children: [
                Obx(
                  () => Checkbox(
                      fillColor: MaterialStateProperty.resolveWith(
                          (_) => ThemeProvider.colorPrimary),
                      side: BorderSide.none,
                      shape: const CircleBorder(
                          side: BorderSide(color: Colors.white)),
                      value: controller.isSelectPaymentCOD.value,
                      onChanged: (value) {
                        controller.isSelectPaymentCOD.value = true;
                      }),
                ),
                Text('cod'.tr),
              ],
            ),
          ).marginOnly(bottom: 5),
          SizedBox(
            height: 20,
            child: Row(
              children: [
                Obx(
                  () => Checkbox(
                      fillColor: MaterialStateProperty.resolveWith(
                          (_) => ThemeProvider.colorPrimary),
                      side: BorderSide.none,
                      shape: const CircleBorder(
                          side: BorderSide(color: Colors.white)),
                      value: !controller.isSelectPaymentCOD.value,
                      onChanged: (value) {
                        controller.isSelectPaymentCOD.value = false;
                      }),
                ),
                Text('pay_ck'.tr),
              ],
            ),
          ).marginOnly(bottom: 10.h),
          Text(
            'shipping_time'.tr,
            style: TextStyle(
                fontStyle: FontStyle.italic,
                fontSize: ThemeProvider.fontSize12),
          ).marginOnly(bottom: 5.h),
          Text(
            'check_product'.tr,
            style: TextStyle(
                fontStyle: FontStyle.italic,
                color: ThemeProvider.colorPrimary,
                fontSize: ThemeProvider.fontSize12),
          )
        ],
      ),
    );
  }

  Widget _buildCustomerInfo() {
    return Container(
      margin: EdgeInsets.only(top: 4.h),
      width: double.maxFinite,
      padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 20.h),
      decoration: BoxDecoration(
        color: Colors.grey.shade200,
        border: Border.all(color: ThemeProvider.colorPrimary),
        borderRadius: const BorderRadius.all(
          Radius.circular(20),
        ),
      ),
      child: Column(
        children: [
          Text(
            'info_shipping'.tr,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ).marginOnly(bottom: 10.h),
          Row(
            children: [
              Expanded(
                  flex: 3,
                  child: Container(
                    margin: EdgeInsets.only(right: 8.w),
                    padding: const EdgeInsets.all(8),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(12),
                      ),
                    ),
                    child: TextFormField(
                      controller: controller.nameController,
                      decoration: InputDecoration(
                        isDense: true,
                        hintText: 'name'.tr,
                        contentPadding: EdgeInsets.zero,
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                      ),
                    ),
                  )),
              Expanded(
                  flex: 2,
                  child: Container(
                    padding: const EdgeInsets.all(8),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(12),
                      ),
                    ),
                    child: TextFormField(
                      controller: controller.phoneController,
                      decoration: InputDecoration(
                        isDense: true,
                        hintText: 'phone'.tr,
                        contentPadding: EdgeInsets.zero,
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                      ),
                    ),
                  ))
            ],
          ).marginOnly(bottom: 10.h),
          Container(
            width: double.maxFinite,
            margin: EdgeInsets.only(right: 8.w),
            padding: const EdgeInsets.all(8),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
            ),
            child: TextFormField(
              controller: controller.addressController,
              decoration: InputDecoration(
                isDense: true,
                hintText: 'address'.tr,
                contentPadding: EdgeInsets.zero,
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
              ),
            ),
          ).marginOnly(bottom: 10.h),
          Container(
            width: double.maxFinite,
            margin: EdgeInsets.only(right: 8.w),
            padding: const EdgeInsets.all(8),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
            ),
            child: TextFormField(
              controller: controller.addressHomeController,
              decoration: InputDecoration(
                isDense: true,
                hintText: 'home_address'.tr,
                contentPadding: EdgeInsets.zero,
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
              ),
            ),
          )
        ],
      ),
    );
  }
}
