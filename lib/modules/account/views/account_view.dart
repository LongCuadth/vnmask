import 'package:app/base/widgets/base_button.dart';
import 'package:app/gen/assets.gen.dart';
import 'package:app/routes/app_pages.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/account_controller.dart';

class AccountView extends GetView<AccountController> {
  const AccountView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        toolbarHeight: 0,
        elevation: 1,
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            _buildInfo().marginOnly(bottom: 20.h),
            _buildMoney().marginOnly(bottom: 20.h),
            _buildShareVoucher().marginOnly(bottom: 20.h),
            _buildSettings(),
            const SizedBox(height: 50)
          ],
        ),
      ),
    );
  }

  Widget _buildInfo() {
    return Column(
      children: [
        SizedBox(width: double.infinity, height: 10.h),
        Text(
          'UserName',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: ThemeProvider.fontSize16,
          ),
        ),
        Text(
          '0398973922',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w300,
            fontSize: ThemeProvider.fontSize14,
          ),
        ).marginOnly(bottom: 5.h),
        const Stack(
          children: [
            CircleAvatar(
              radius: 50,
              backgroundColor: ThemeProvider.colorPrimary,
              child: Icon(Icons.image_sharp),
            ),
            Positioned(
              right: 2,
              bottom: 2,
              child: CircleAvatar(
                radius: 12,
                backgroundColor: Colors.green,
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                  size: 20,
                ),
              ),
            ),
          ],
        ).marginOnly(bottom: 5.h),
        Text(
          'fanpay.co@gmail.com',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: ThemeProvider.fontSize14,
          ),
        ),
      ],
    );
  }

  Widget _buildMoney() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16.w),
      height: 40,
      padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 2.h),
      decoration: BoxDecoration(
        color: Colors.grey.shade200,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(20)),
            child: Assets.image.logo.image(fit: BoxFit.cover),
          ),
          Text(
            'total_money_wallet'.tr,
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w400,
              fontSize: ThemeProvider.fontSize12,
            ),
          ).marginSymmetric(horizontal: 8.w),
          Text(
            '1.550.000đ',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: ThemeProvider.fontSize12,
            ),
          ),
          const Spacer(),
          BaseButton(
              width: 100,
              height: 25,
              radius: 8,
              title: 'add_money_wallet'.tr,
              onPressed: () {
                Get.toNamed(Routes.MONEY_WALLET);
              },
              textStyle: TextStyle(
                  color: Colors.white,
                  fontSize: ThemeProvider.fontSize12,
                  fontWeight: FontWeight.bold),
              styleButton: BaseButtonStyle.fill)
        ],
      ),
    );
  }

  Widget _buildShareVoucher() {
    return Row(
      children: [
        Expanded(
          child: GestureDetector(
            onTap: () {
              Get.toNamed(Routes.SHARE_APP);
            },
            child: Container(
              margin: EdgeInsets.only(left: 16.w),
              height: 40,
              padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 2.h),
              decoration: BoxDecoration(
                color: Colors.grey.shade200,
                borderRadius: BorderRadius.circular(8),
              ),
              child: Row(
                children: [
                  const CircleAvatar(
                    radius: 10,
                    backgroundColor: Colors.orange,
                    child: Icon(
                      Icons.share,
                      size: 12,
                      color: Colors.white,
                    ),
                  ),
                  const Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      RichText(
                        text: TextSpan(
                            text: 'introduce'.tr,
                            style: TextStyle(
                                color: ThemeProvider.colorTextBlack,
                                fontSize: ThemeProvider.fontSize12),
                            children: [
                              TextSpan(
                                text: ' VN',
                                style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w500,
                                    fontSize: ThemeProvider.fontSize12),
                              ),
                              TextSpan(
                                text: 'MASK',
                                style: TextStyle(
                                    color: const Color(0xff3171b9),
                                    fontWeight: FontWeight.w500,
                                    fontSize: ThemeProvider.fontSize12),
                              ),
                            ]),
                      ).marginOnly(left: 8.w, top: 8.h),
                      Text(
                        'received_100'.tr,
                        style: TextStyle(
                            color: ThemeProvider.colorPrimary,
                            fontWeight: FontWeight.w500,
                            fontSize: ThemeProvider.fontSize12),
                      )
                    ],
                  ),
                  const Spacer(),
                  const Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.grey,
                    size: 14,
                  )
                ],
              ),
            ),
          ),
        ),
        SizedBox(width: 8.w),
        Expanded(
            child: GestureDetector(
          onTap: () {
            Get.toNamed(Routes.VOUCHER_MANAGER);
          },
          child: Container(
            margin: EdgeInsets.only(right: 16.w),
            height: 40,
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 2.h),
            decoration: BoxDecoration(
              color: Colors.grey.shade200,
              borderRadius: BorderRadius.circular(8),
            ),
            child: Row(
              children: [
                CircleAvatar(
                  radius: 10,
                  backgroundColor: Colors.grey.shade200,
                  child: const Icon(
                    Icons.card_giftcard,
                    size: 20,
                    color: Colors.red,
                  ),
                ),
                const Spacer(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'voucher_manager'.tr,
                      style: TextStyle(
                          color: ThemeProvider.colorBlack,
                          fontWeight: FontWeight.w500,
                          fontSize: ThemeProvider.fontSize12),
                    ).marginOnly(left: 8.w, top: 8.h),
                    Text(
                      '10_voucher'.tr,
                      style: TextStyle(
                          color: ThemeProvider.colorPrimary,
                          fontWeight: FontWeight.w500,
                          fontSize: ThemeProvider.fontSize12),
                    )
                  ],
                ),
                const Spacer(),
                const Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.grey,
                  size: 14,
                )
              ],
            ),
          ),
        ))
      ],
    );
  }

  Widget _buildSettings() {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 16.w),
      padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 20.h),
      decoration: BoxDecoration(
        color: Colors.grey.shade200,
        border: Border.all(color: Colors.grey, width: 0.7),
        borderRadius: BorderRadius.circular(12),
      ),
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              Get.toNamed(Routes.SETTING_ACCOUNT);
            },
            child: buildItemSetting(
                const Icon(Icons.person, color: Colors.blueAccent),
                'setting_account'.tr),
          ),
          buildItemSetting(
              const Icon(Icons.location_on, color: Colors.redAccent),
              'settings_address'.tr),
          buildItemSetting(
              const Icon(Icons.settings_sharp, color: Colors.blueAccent),
              'setting_password'.tr),
          buildItemSetting(
              const Icon(Icons.notifications, color: Colors.orangeAccent),
              'setting_noti'.tr),
          buildItemSetting(
              const Icon(Icons.video_camera_back_outlined, color: Colors.red),
              'guide_youtube'.tr),
          buildItemSetting(
              Assets.image.zalo.image(height: 25), 'zalo_support'.tr),
          buildItemSetting(
              Assets.image.messenger.image(height: 25), 'messenger_support'.tr),
          buildItemSetting(const Icon(Icons.language), 'setting_language'.tr),
          SizedBox(
            height: 140.h,
          ),
          BaseButton(
              width: 200,
              height: 35,
              radius: 8,
              title: 'logout'.tr,
              onPressed: () {
                Get.offAllNamed(Routes.LOGIN);
              },
              textStyle: TextStyle(
                  color: Colors.white,
                  fontSize: ThemeProvider.fontSize14,
                  fontWeight: FontWeight.bold),
              styleButton: BaseButtonStyle.fill)
        ],
      ),
    );
  }

  Widget buildItemSetting(Widget leading, String title) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: const BoxDecoration(
          color: Colors.white,
          border: Border(bottom: BorderSide(color: Colors.grey, width: 0.5))),
      child: Row(
        children: [
          leading,
          const SizedBox(
            width: 20,
          ),
          Text(title),
          const Spacer(),
          const Icon(
            Icons.arrow_forward_ios,
            size: 16,
            color: Colors.grey,
          )
        ],
      ),
    );
  }
}
