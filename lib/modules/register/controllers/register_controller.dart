import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class RegisterController extends GetxController {
  var phoneController = TextEditingController();
  var nameController = TextEditingController();
  var gmailController = TextEditingController();
  var inviteController = TextEditingController();
  var passwordController = TextEditingController();
  var rePasswordController = TextEditingController();
}
