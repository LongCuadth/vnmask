import 'package:app/base/widgets/base_button.dart';
import 'package:app/base/widgets/base_textfield.dart';
import 'package:app/gen/assets.gen.dart';
import 'package:app/routes/app_pages.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/register_controller.dart';

class RegisterView extends GetView<RegisterController> {
  const RegisterView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeProvider.colorWhite,
      body: SafeArea(
          child: Stack(
        children: [
          Positioned.fill(
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              physics: const BouncingScrollPhysics(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 30.h, width: double.maxFinite),
                  Assets.image.logo
                      .image(width: 100.w)
                      .marginOnly(bottom: 20.h),
                  Text(
                    'register'.tr,
                    style: TextStyle(
                        fontSize: ThemeProvider.fontSize18,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ).marginOnly(bottom: 40.h),
                  BaseTextField(
                          textEditingController: controller.nameController,
                          hintText: 'name'.tr)
                      .marginSymmetric(horizontal: 50.w, vertical: 10.h),
                  BaseTextField(
                          textEditingController: controller.phoneController,
                          hintText: 'phone'.tr)
                      .marginSymmetric(horizontal: 50.w, vertical: 10.h),
                  BaseTextField(
                          textEditingController: controller.gmailController,
                          hintText: 'gmail'.tr)
                      .marginSymmetric(horizontal: 50.w, vertical: 10.h),
                  BaseTextField(
                          isPassword: true,
                          textEditingController: controller.passwordController,
                          hintText: 'password'.tr)
                      .marginSymmetric(horizontal: 50.w, vertical: 10.h),
                  BaseTextField(
                          isPassword: true,
                          textEditingController:
                              controller.rePasswordController,
                          hintText: 'repassword'.tr)
                      .marginSymmetric(horizontal: 50.w, vertical: 10.h),
                  BaseTextField(
                          textEditingController: controller.inviteController,
                          hintText: 'invite'.tr)
                      .marginSymmetric(horizontal: 50.w, vertical: 10.h)
                      .marginOnly(bottom: 20.h),
                  SizedBox(
                    width: 200.w,
                    child: BaseButton(
                        height: 30,
                        title: 'continue'.tr,
                        onPressed: () {
                          Get.toNamed(Routes.ACTIVE_ACCOUNT);
                        },
                        radius: 8,
                        textStyle: TextStyle(
                            fontSize: ThemeProvider.fontSize18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                        styleButton: BaseButtonStyle.fill),
                  )
                ],
              ),
            ),
          ),
          Positioned(
              left: 10,
              child: InkWell(
                onTap: () {
                  Get.back();
                },
                child: const SizedBox(
                  child: Icon(
                    Icons.keyboard_double_arrow_left,
                    size: 34,
                    color: Colors.black,
                  ),
                ),
              ))
        ],
      )),
    );
  }
}
