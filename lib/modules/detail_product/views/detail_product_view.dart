import 'package:app/base/widgets/base_button.dart';
import 'package:app/modules/home/widgets/add_to_cart_widget.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/detail_product_controller.dart';

class DetailProductView extends GetView<DetailProductController> {
  const DetailProductView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 0,
          elevation: 1,
        ),
        body: SafeArea(
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              SingleChildScrollView(
                padding: EdgeInsets.only(bottom: 100.h),
                child: Column(
                  children: [
                    CachedNetworkImage(
                      imageUrl:
                          'https://content.pancake.vn/1/s750x750/fwebp/f0/d8/39/5c/cf01e5b50bef662fda41584a977bc02cfa5a667c13b3215bef229b2f.jpg',
                      width: double.maxFinite,
                      imageBuilder: (context, imageProvider) => Container(
                        width: double.infinity,
                        height: 250,
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: DecorationImage(
                              image: imageProvider, fit: BoxFit.fill),
                        ),
                      ),
                    ).marginOnly(bottom: 5.h),
                    Container(
                      padding: const EdgeInsets.all(8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Combo 100 cái khẩu trang 5D mask Minh Hiếu 3 lớp cao cấp chống bụi mịn và kháng khuẩn 99+',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w600),
                          ).marginOnly(bottom: 10.h),
                          Row(children: [
                            Text(
                              '55.000đ - 105.00đ',
                              style: TextStyle(
                                  color: Colors.red,
                                  fontSize: ThemeProvider.fontSize20,
                                  fontWeight: FontWeight.w600),
                            ),
                            const Spacer(),
                            const Icon(
                              Icons.star,
                              color: Colors.amber,
                              size: 12,
                            ),
                            const Icon(
                              Icons.star,
                              color: Colors.amber,
                              size: 12,
                            ),
                            const Icon(
                              Icons.star,
                              color: Colors.amber,
                              size: 12,
                            ),
                            const Icon(
                              Icons.star,
                              color: Colors.amber,
                              size: 12,
                            ),
                            const Icon(
                              Icons.star,
                              color: Colors.grey,
                              size: 12,
                            ),
                            const SizedBox(width: 10),
                            Text(
                              'Đã bán 10,2k',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: ThemeProvider.fontSize12),
                            )
                          ]),
                          const SizedBox(height: 10),
                          Text(
                            'Mô tả sản phẩm',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: ThemeProvider.fontSize14,
                                fontWeight: FontWeight.w600),
                          ),
                          const SizedBox(height: 10),
                          Text(
                            '''
[Combo Sỉ 100 Cái] Khẩu trang 6D PRO UNICARE, phiên bản cao cấp chính hãng UNI MASK
Thông tin sản phẩm :
* Thương hiệu: UNI MASK_UNICARE
* Xuất xứ: Việt Nam
* Quy cách đóng gói: 1túi/ 10 chiếc
* Kích thước khẩu trang: 20 cm x 9 cm
* Màu sắc: Trắng, Đen, Xám, Nâu hạt dẻ, Hồng Phấn, Hồng Sim
* Thành phần:
+ Cấu trúc 4 lớp kháng khuẩn. Gồm vải không dệt chất lượng cao, lớp vi lọc an toàn cho sức khỏe.
+ Lớp kháng khuẩn trong khẩu trang được kiểm chứng bởi các phòng thí nghiệm uy tín thế giới.
+ Dây thun đeo tai mềm mại, đàn hồi tốt.
+ Form ôm trọn khuôn mặt
--------------
*Hướng dẫn sử dụng:
+ Khẩu trang 6D sử dụng một lần.
+ Ngăn giọt bắn, chống khói, bụi mịn.
+ Mở gói, lấy khẩu trang ra khỏi gói nhẹ nhàng, cầm 2 mép khẩu trang tách nhẹ
+ Đeo hai quai vào 2 bên tại
*Hướng dẫn bảo quản: Nơi khô ráo, thoáng mát
--------------
Đạt chuẩn:
• Bộ Y Tế Việt Nam
• Chất lượng đã được kiểm chứng
''',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: ThemeProvider.fontSize14,
                                fontWeight: FontWeight.w300),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                padding: EdgeInsets.all(20.h),
                decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    border:
                        Border(top: BorderSide(color: Colors.grey.shade400))),
                height: 100.h,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    BaseButton(
                        width: 80,
                        radius: 8,
                        backgroundColor: Colors.teal,
                        title: 'support'.tr,
                        onPressed: () {},
                        styleButton: BaseButtonStyle.fill),
                    BaseButton(
                        width: 130,
                        radius: 8,
                        backgroundColor: Colors.orange,
                        title: 'add_to_cart'.tr,
                        onPressed: () {
                          Get.bottomSheet(const AddToCartWidget(),
                              isDismissible: false, isScrollControlled: true);
                        },
                        styleButton: BaseButtonStyle.fill),
                    BaseButton(
                        width: 120,
                        radius: 8,
                        backgroundColor: ThemeProvider.colorPrimary,
                        title: 'buy_now'.tr,
                        onPressed: () {
                          Get.bottomSheet(
                              const AddToCartWidget(
                                isBuyNow: true,
                              ),
                              isDismissible: false,
                              isScrollControlled: true);
                        },
                        styleButton: BaseButtonStyle.fill),
                  ],
                ),
              ),
              Positioned(
                top: 10,
                left: 10,
                child: GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                    child: const Center(
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 10,
                right: 10,
                child: GestureDetector(
                  onTap: () {},
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                    child: const Center(
                      child: Icon(
                        Icons.ios_share_outlined,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
