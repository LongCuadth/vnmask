import 'package:app/base/widgets/base_button.dart';
import 'package:app/gen/assets.gen.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/share_app_controller.dart';

class ShareAppView extends GetView<ShareAppController> {
  const ShareAppView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('introduce'.tr),
        centerTitle: true,
        elevation: 2,
        leading: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: const Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
      ),
      body: SafeArea(
          child: Column(
        children: [
          Center(
              child: Assets.image.logo.image(width: Get.width, height: 150.h)),
          Container(
            width: Get.width,
            margin: EdgeInsets.symmetric(horizontal: 16.w),
            decoration: BoxDecoration(
                color: Colors.grey.shade200,
                borderRadius: BorderRadius.circular(16)),
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Column(
              children: [
                Text('code_share'.tr).marginOnly(bottom: 20.h),
                Container(
                  width: double.maxFinite,
                  margin: EdgeInsets.symmetric(horizontal: 16.w),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(16),
                      border: Border.all(color: Colors.grey, width: 0.5)),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Center(
                      child: Text(
                    '0398973922',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: ThemeProvider.fontSize18,
                        fontWeight: FontWeight.bold),
                  )),
                ).marginOnly(bottom: 10.h),
                Row(
                  children: [
                    Expanded(
                        child: BaseButton(
                            title: 'send_sms'.tr,
                            textStyle: TextStyle(
                                color: Colors.white,
                                fontSize: ThemeProvider.fontSize16,
                                fontWeight: FontWeight.bold),
                            onPressed: () {},
                            radius: 8,
                            backgroundColor: Colors.green,
                            styleButton: BaseButtonStyle.fill)),
                    SizedBox(width: 20.w),
                    Expanded(
                        child: BaseButton(
                            title: 'share'.tr,
                            onPressed: () {},
                            radius: 8,
                            textStyle: TextStyle(
                                color: Colors.white,
                                fontSize: ThemeProvider.fontSize16,
                                fontWeight: FontWeight.bold),
                            styleButton: BaseButtonStyle.fill))
                  ],
                )
              ],
            ),
          ).marginOnly(bottom: 20.h),
          Text(
            'Chỉ cần giới thiệu VNMASK & hướng dẫn bạn bè hoàn thành các nhiệm vụ đơn giản, bạn sẽ nhận được phần quà 10 mã Voucher trị giá 100.000đ, Thực hiện ngay kẻo lỡ!',
            style:
                TextStyle(fontSize: ThemeProvider.fontSize12 + 1, height: 1.2),
          ).marginSymmetric(horizontal: 16.h).marginOnly(bottom: 10.h),
          Text(
            'Thời gian chương trình: Từ 12h00 ngày 27/10/2023 đến khi có thông báo mới.',
            style:
                TextStyle(fontSize: ThemeProvider.fontSize12 + 1, height: 1.2),
          ).marginSymmetric(horizontal: 16.h).marginOnly(bottom: 10.h),
          Text(
            '''Giới thiệu bạn bè thành công, nhận trọn bộ quà 10 mã Voucher trị giá 100.000đ thanh toán dịch vụ khi mua hàng trên 

Hướng dẫn bạn bè hoàn thành các bước đơn giản sau:''',
            style:
                TextStyle(fontSize: ThemeProvider.fontSize12 + 1, height: 1.2),
          ).marginSymmetric(horizontal: 16.h).marginOnly(bottom: 10.h),
          Text(
            '''- Bước 1: Tải ứng dụng VNMASK và đăng ký tài khoản;
- Bước 2: Nhập mã giới thiệu (số điện thoại của người giới thiệu)
- Bước 3: Thực hiện tối thiểu 5 giao dịch trên VNMASK

Sau khi bạn bè hoàn thành các bước VNMASK sẽ kích hoạt phần quà 10 mã Voucher trị giá 100.000đ tới tài khoản của bạn.''',
            style:
                TextStyle(fontSize: ThemeProvider.fontSize12 + 1, height: 1.2),
          ).marginSymmetric(horizontal: 16.h).marginOnly(bottom: 12.h),
          Text(
            'Rủ bạn càng đông quà về càng nhiều.',
            style:
                TextStyle(fontSize: ThemeProvider.fontSize12 + 1, height: 1.2),
          ).marginSymmetric(horizontal: 16.h).marginOnly(bottom: 10.h),
        ],
      )),
    );
  }
}
