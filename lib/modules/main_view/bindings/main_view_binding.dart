import 'package:app/modules/account/bindings/account_binding.dart';
import 'package:app/modules/cart/bindings/cart_binding.dart';
import 'package:app/modules/home/bindings/home_binding.dart';
import 'package:app/modules/phone_recharge/bindings/phone_recharge_binding.dart';
import 'package:app/modules/transaction_history/bindings/transaction_history_binding.dart';
import 'package:get/get.dart';

import '../controllers/main_view_controller.dart';

class MainViewBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MainViewController>(
      () => MainViewController(),
    );
    PhoneRechargeBinding().dependencies();
    CartBinding().dependencies();
    HomeBinding().dependencies();
    TransactionHistoryBinding().dependencies();
    AccountBinding().dependencies();
  }
}
