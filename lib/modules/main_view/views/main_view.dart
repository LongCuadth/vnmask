import 'package:app/modules/account/views/account_view.dart';
import 'package:app/modules/cart/views/cart_view.dart';
import 'package:app/modules/home/views/home_view.dart';
import 'package:app/modules/phone_recharge/views/phone_recharge_view.dart';
import 'package:app/modules/transaction_history/views/transaction_history_view.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:curved_labeled_navigation_bar/curved_navigation_bar.dart';
import 'package:curved_labeled_navigation_bar/curved_navigation_bar_item.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/main_view_controller.dart';

class MainView extends GetView<MainViewController> {
  const MainView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: PageView(
        controller: controller.pageController,
        physics: const NeverScrollableScrollPhysics(),
        children: const [
          PhoneRechargeView(),
          CartView(),
          HomeView(),
          TransactionHistoryView(),
          AccountView()
        ],
      ),
      bottomNavigationBar: buildBottomBar(),
    );
  }

  Obx buildBottomBar() {
    return Obx(
      () => CurvedNavigationBar(
        index: controller.currentIndex.value,
        color: const Color(0xfff7f7f7),
        backgroundColor: Colors.white,
        buttonBackgroundColor: Colors.red,
        items: [
          CurvedNavigationBarItem(
              child: Icon(
                Icons.phone_android_rounded,
                color: controller.currentIndex.value == 0
                    ? Colors.white
                    : Colors.black,
              ),
              label: 'dt'.tr,
              labelStyle: TextStyle(
                fontSize: ThemeProvider.fontSize12 + 1,
                color: controller.currentIndex.value == 0
                    ? Colors.red
                    : Colors.black,
              )),
          CurvedNavigationBarItem(
              child: Icon(
                Icons.shopping_cart_outlined,
                color: controller.currentIndex.value == 1
                    ? Colors.white
                    : Colors.black,
              ),
              label: 'cart'.tr,
              labelStyle: TextStyle(
                fontSize: ThemeProvider.fontSize12 + 1,
                color: controller.currentIndex.value == 1
                    ? Colors.red
                    : Colors.black,
              )),
          CurvedNavigationBarItem(
              child: Icon(
                Icons.home_filled,
                color: controller.currentIndex.value == 2
                    ? Colors.white
                    : Colors.black,
              ),
              label: 'home'.tr,
              labelStyle: TextStyle(
                fontSize: ThemeProvider.fontSize12 + 1,
                color: controller.currentIndex.value == 2
                    ? Colors.red
                    : Colors.black,
              )),
          CurvedNavigationBarItem(
              child: Icon(
                Icons.sync_outlined,
                color: controller.currentIndex.value == 3
                    ? Colors.white
                    : Colors.black,
              ),
              label: 'history'.tr,
              labelStyle: TextStyle(
                fontSize: ThemeProvider.fontSize12 + 1,
                color: controller.currentIndex.value == 3
                    ? Colors.red
                    : Colors.black,
              )),
          CurvedNavigationBarItem(
              child: Icon(
                Icons.account_circle,
                color: controller.currentIndex.value == 4
                    ? Colors.white
                    : Colors.black,
              ),
              label: 'account'.tr,
              labelStyle: TextStyle(
                fontSize: ThemeProvider.fontSize12 + 1,
                color: controller.currentIndex.value == 4
                    ? Colors.red
                    : Colors.black,
              )),
        ],
        onTap: (index) {
          controller.currentIndex.value = index;
          controller.pageController.animateToPage(index,
              duration: const Duration(milliseconds: 600),
              curve: Curves.easeOut);
        },
      ),
    );
  }
}
