import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainViewController extends GetxController {
  final currentIndex = 2.obs;

  var pageController = PageController(initialPage: 2, keepPage: true);

  void jumpToPage(int i) {
    currentIndex.value = i;
    pageController.jumpToPage(i);
  }
}
