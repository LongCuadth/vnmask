import 'package:get/get.dart';

import '../controllers/phone_recharge_controller.dart';

class PhoneRechargeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PhoneRechargeController>(
      () => PhoneRechargeController(),
    );
  }
}
