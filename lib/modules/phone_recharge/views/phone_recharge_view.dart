import 'package:app/base/widgets/base_button.dart';
import 'package:app/gen/assets.gen.dart';
import 'package:app/modules/home/widgets/item_select_type.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/phone_recharge_controller.dart';

class PhoneRechargeView extends GetView<PhoneRechargeController> {
  const PhoneRechargeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ThemeProvider.colorPrimary,
          centerTitle: true,
          elevation: 0,
          title: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Số dư: '),
              Text(
                '1.550.000 đ',
                style: TextStyle(fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        body: SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 20.h),
            child: Column(
              children: [
                BaseButton(
                        height: 32.h,
                        width: 180.w,
                        title: 'add_money'.tr,
                        onPressed: () {},
                        radius: 8,
                        styleButton: BaseButtonStyle.fill)
                    .marginOnly(bottom: 20),
                Obx(
                  () => Row(
                    children: [
                      Expanded(
                          child: GestureDetector(
                        onTap: () {
                          controller.hostType.value = HostType.viettel;
                        },
                        child: ItemHostType(
                          sale: '5',
                          image: Assets.image.viettel.image(height: 30),
                          isSelected:
                              controller.hostType.value == HostType.viettel,
                        ),
                      )),
                      SizedBox(width: 10.w),
                      Expanded(
                          child: GestureDetector(
                              onTap: () {
                                controller.hostType.value = HostType.vina;
                              },
                              child: ItemHostType(
                                sale: '5',
                                image: Assets.image.vina
                                    .image(width: 120, height: 120),
                                isSelected:
                                    controller.hostType.value == HostType.vina,
                              ))),
                      SizedBox(width: 10.w),
                      Expanded(
                          child: GestureDetector(
                              onTap: () {
                                controller.hostType.value = HostType.mobi;
                              },
                              child: ItemHostType(
                                sale: '5',
                                image: Assets.image.mobi.image(height: 20),
                                isSelected:
                                    controller.hostType.value == HostType.mobi,
                              ))),
                    ],
                  ),
                ).marginOnly(bottom: 15.h),
                Obx(
                  () => Row(
                    children: [
                      Expanded(
                          child: GestureDetector(
                        onTap: () {
                          controller.payType.value = PayType.before;
                        },
                        child: ItemPayType(
                          title: 'pay_before'.tr,
                          isSelected:
                              controller.payType.value == PayType.before,
                        ),
                      )),
                      SizedBox(width: 10.w),
                      Expanded(
                          child: GestureDetector(
                              onTap: () {
                                controller.payType.value = PayType.after;
                              },
                              child: ItemPayType(
                                  title: 'pay_after'.tr,
                                  isSelected: controller.payType.value ==
                                      PayType.after))),
                    ],
                  ).marginSymmetric(horizontal: 50.w),
                ).marginOnly(bottom: 20.h),
                Text('input_phone'.tr),
                TextFormField(
                  decoration: InputDecoration(hintText: 'phone'.tr),
                ).marginSymmetric(horizontal: 50.w).marginOnly(bottom: 10.h),
                TextFormField(
                  decoration: InputDecoration(
                    prefixIcon: const Text(
                      'OTP:',
                    ).marginOnly(top: 20.h),
                  ),
                ).marginSymmetric(horizontal: 80.w).marginOnly(bottom: 10.h),
                Obx(() => Row(
                      children: [
                        Expanded(
                            child: GestureDetector(
                          onTap: () {
                            controller.moneyType.value = MoneyType.fifty;
                          },
                          child: ItemMoneyType(
                              price: '50.000'.tr,
                              sale: '45.000'.tr,
                              isSelected: controller.moneyType.value ==
                                  MoneyType.fifty),
                        )),
                        SizedBox(width: 4.w),
                        Expanded(
                            child: GestureDetector(
                          onTap: () {
                            controller.moneyType.value = MoneyType.oneHundred;
                          },
                          child: ItemMoneyType(
                              price: '100.000'.tr,
                              sale: '95.000'.tr,
                              isSelected: controller.moneyType.value ==
                                  MoneyType.oneHundred),
                        )),
                        SizedBox(width: 4.w),
                        Expanded(
                            child: GestureDetector(
                          onTap: () {
                            controller.moneyType.value = MoneyType.twoHundred;
                          },
                          child: ItemMoneyType(
                              price: '200.000'.tr,
                              sale: '195.000'.tr,
                              isSelected: controller.moneyType.value ==
                                  MoneyType.twoHundred),
                        )),
                        SizedBox(width: 4.w),
                        Expanded(
                            child: GestureDetector(
                          onTap: () {
                            controller.moneyType.value = MoneyType.fiveHundred;
                          },
                          child: ItemMoneyType(
                              price: '500.000'.tr,
                              sale: '495.000'.tr,
                              isSelected: controller.moneyType.value ==
                                  MoneyType.fiveHundred),
                        )),
                      ],
                    )).marginOnly(bottom: 20.h),
                SizedBox(height: 150.h),
                BaseButton(
                        height: 40.h,
                        width: 230.w,
                        title: 'confirm'.tr,
                        textStyle: const TextStyle(
                            fontWeight: FontWeight.w600,
                            color: ThemeProvider.colorWhite),
                        onPressed: () {},
                        radius: 8,
                        styleButton: BaseButtonStyle.fill)
                    .marginOnly(bottom: 20),
              ],
            )));
  }
}
