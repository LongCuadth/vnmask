import 'package:get/get.dart';

enum HostType { vina, viettel, mobi }

enum PayType { before, after }

enum MoneyType { fifty, oneHundred, twoHundred, fiveHundred }

class PhoneRechargeController extends GetxController {
  var hostType = HostType.viettel.obs;
  var payType = PayType.before.obs;
  var moneyType = MoneyType.fifty.obs;
}
