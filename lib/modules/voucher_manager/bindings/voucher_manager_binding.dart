import 'package:get/get.dart';

import '../controllers/voucher_manager_controller.dart';

class VoucherManagerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<VoucherManagerController>(
      () => VoucherManagerController(),
    );
  }
}
