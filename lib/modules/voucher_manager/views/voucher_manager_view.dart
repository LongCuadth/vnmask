import 'package:app/base/widgets/base_button.dart';
import 'package:app/gen/assets.gen.dart';
import 'package:app/modules/home/widgets/item_select_type.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/voucher_manager_controller.dart';

class VoucherManagerView extends GetView<VoucherManagerController> {
  const VoucherManagerView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          toolbarHeight: 0,
          elevation: 0,
        ),
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _buildHeader().marginOnly(top: 5),
              Expanded(
                  child: Column(
                children: [
                  Obx(
                    () => Container(
                      padding: const EdgeInsets.all(12),
                      height: 100,
                      child: Column(children: [
                        Row(
                          children: [
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  controller.voucherType.value =
                                      VoucherType.active;
                                },
                                child: ItemSelectType(
                                  title: 'active'.tr,
                                  isSelected: controller.voucherType.value ==
                                      VoucherType.active,
                                ),
                              ).marginOnly(right: 10.w),
                            ),
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  controller.voucherType.value =
                                      VoucherType.inactive;
                                },
                                child: ItemSelectType(
                                  title: 'inactive'.tr,
                                  isSelected: controller.voucherType.value ==
                                      VoucherType.inactive,
                                ),
                              ).marginOnly(right: 10.w),
                            ),
                          ],
                        ).marginOnly(bottom: 12.h),
                        Row(
                          children: [
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  controller.voucherType.value =
                                      VoucherType.wait;
                                },
                                child: ItemSelectType(
                                  title: 'waiting'.tr,
                                  isSelected: controller.voucherType.value ==
                                      VoucherType.wait,
                                ),
                              ).marginOnly(right: 10.w),
                            ),
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  controller.voucherType.value =
                                      VoucherType.used;
                                },
                                child: ItemSelectType(
                                  title: 'used'.tr,
                                  isSelected: controller.voucherType.value ==
                                      VoucherType.used,
                                ),
                              ).marginOnly(right: 10.w),
                            ),
                          ],
                        )
                      ]),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.grey.shade200,
                      child: Obx(
                        () => Visibility(
                          visible: controller.voucherType.value is VoucherType,
                          child: ListView.builder(
                            itemCount: 10,
                            itemBuilder: (context, index) {
                              return controller.voucherType.value ==
                                      VoucherType.active
                                  ? Obx(
                                      () => buildItemActive(index, true),
                                    )
                                  : Obx(
                                      () => buildItemActive(index, false),
                                    );
                            },
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              )),
              Obx(() => Visibility(
                  visible: controller.voucherType.value == VoucherType.active,
                  child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.only(top: 5.h),
                      height: 60,
                      color: Colors.grey.shade200,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Voucher đã được chọn giảm 10k',
                            style: TextStyle(
                                color: Colors.green,
                                fontSize: ThemeProvider.fontSize12),
                          ).marginOnly(bottom: 5.h),
                          BaseButton(
                              width: 200,
                              height: 30,
                              title: 'agree'.tr,
                              textStyle: TextStyle(
                                  color: Colors.white,
                                  fontSize: ThemeProvider.fontSize16,
                                  fontWeight: FontWeight.bold),
                              onPressed: () {},
                              radius: 8,
                              backgroundColor: ThemeProvider.colorPrimary,
                              styleButton: BaseButtonStyle.fill),
                        ],
                      ))))
            ],
          ),
        ));
  }

  Container buildItemActive(int index, bool isActive) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
      decoration: BoxDecoration(
          color: isActive ? Colors.white : Colors.grey.shade400,
          borderRadius: BorderRadius.circular(16)),
      margin: const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Assets.image.voucher.image(width: 30).marginOnly(right: 10),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      'Giảm 10k ',
                      style: TextStyle(
                          fontSize: ThemeProvider.fontSize12,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'Từ số điện thoại bạn giới thiệu 0398973922',
                      style: TextStyle(
                        fontSize: ThemeProvider.fontSize12,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Icon(
                      Icons.access_time_rounded,
                      size: 14,
                    ).marginOnly(right: isActive ? 40 : 4),
                    Expanded(
                      child: Text(
                        'Còn hiệu lực 29 ngày',
                        style: TextStyle(
                            fontSize: ThemeProvider.fontSize12,
                            color: isActive
                                ? Colors.blue
                                : ThemeProvider.colorPrimary),
                      ),
                    ),
                    isActive
                        ? Align(
                            alignment: Alignment.centerRight,
                            child: SizedBox(
                                width: 24,
                                height: 24,
                                child: Checkbox(
                                    fillColor:
                                        MaterialStateProperty.resolveWith(
                                            (_) => ThemeProvider.colorPrimary),
                                    shape: const CircleBorder(),
                                    value: controller.voucherType.value ==
                                            VoucherType.active &&
                                        index == 0,
                                    onChanged: (value) {})),
                          )
                        : Expanded(
                            child: Text(
                              'Thiếu 5 giao dịch để kích hoạt',
                              style: TextStyle(
                                  fontSize: ThemeProvider.fontSize12,
                                  color: Colors.blue),
                            ),
                          ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHeader() {
    return Stack(children: [
      SizedBox(
        width: double.infinity,
        height: 30,
        child: Center(
            child: Text(
          'voucher'.tr,
          style: TextStyle(
              color: Colors.black,
              fontSize: ThemeProvider.fontSize18,
              fontWeight: FontWeight.w600),
        )),
      ),
      Align(
        alignment: Alignment.centerLeft,
        child: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
            child: const Icon(
              Icons.arrow_back_ios,
            ),
          ),
        ),
      )
    ]);
  }
}
