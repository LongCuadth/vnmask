import 'package:app/base/widgets/base_button.dart';
import 'package:app/routes/app_pages.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../controllers/list_order_controller.dart';

class ListOrderView extends GetView<ListOrderController> {
  const ListOrderView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 2,
          backgroundColor: Colors.white,
          title: Obx(() => Text(
                controller.name.value,
                style: const TextStyle(color: Colors.black),
              )),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: Obx(
          () => Visibility(
            visible: controller.name.isNotEmpty,
            child: ListView.builder(
              itemCount: 2,
              itemBuilder: (context, index) {
                switch (controller.index.value) {
                  case 0:
                    return buildItemWaittingOrder(index);
                  case 1:
                    return buildItemShippingOrder(index);
                  case 2:
                    return buildItemDoneOrder(index);
                  case 3:
                    return buildItemCancelOrder(index);

                  default:
                    return Container();
                }
              },
            ),
          ),
        ));
  }

  Widget buildItemDoneOrder(int index) {
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.all(4),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.grey, width: 0.9),
          borderRadius: const BorderRadius.all(Radius.circular(20))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Mã giao dịch: 952$index',
              ),
              Container(
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                    color: Colors.grey.shade200,
                    borderRadius: BorderRadius.circular(12)),
                child: Text(
                  'Hành trình đơn hàng',
                  style: TextStyle(
                      color: Colors.red, fontSize: ThemeProvider.fontSize12),
                ),
              )
            ],
          ).marginOnly(bottom: 5),
          Text(
            'order_done'.tr,
            style: TextStyle(
                fontSize: ThemeProvider.fontSize12, color: Colors.green),
          ).marginOnly(bottom: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 60,
                height: 60,
                child: CachedNetworkImage(
                  imageUrl:
                      'https://content.pancake.vn/1/s750x750/fwebp/f0/d8/39/5c/cf01e5b50bef662fda41584a977bc02cfa5a667c13b3215bef229b2f.jpg',
                ),
              ).marginOnly(right: 10),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('Thùng 100 Cái khẩu trang')
                      .marginOnly(bottom: 4.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'loại',
                        style: TextStyle(fontSize: ThemeProvider.fontSize12),
                      ),
                      Text(
                        'x số lượng',
                        style: TextStyle(fontSize: ThemeProvider.fontSize12),
                      )
                    ],
                  ).marginOnly(bottom: 4.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        'đ 50.000',
                        style: TextStyle(
                          fontSize: ThemeProvider.fontSize14,
                          decoration: TextDecoration.lineThrough,
                        ),
                      ).marginOnly(right: 10.w),
                      Text(
                        'đ 40.000',
                        style: TextStyle(
                            fontSize: ThemeProvider.fontSize14,
                            color: Colors.red),
                      )
                    ],
                  ),
                ],
              ))
            ],
          ),
          const Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('1 sản phẩm'),
              Row(
                children: [
                  const Text('Tổng thanh toán:'),
                  Text(
                    'đ 40.000',
                    style: TextStyle(
                        fontSize: ThemeProvider.fontSize16, color: Colors.red),
                  ),
                ],
              )
            ],
          ),
          const Divider(),
          Align(
              alignment: Alignment.centerRight,
              child: index % 2 == 0
                  ? Text('cod'.tr)
                  : Text(
                      'payment_ck'.tr,
                      style: const TextStyle(
                          color: Colors.green, fontStyle: FontStyle.italic),
                    )),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              BaseButton(
                      width: 180.w,
                      height: 60.h,
                      radius: 8,
                      backgroundColor: Colors.grey.shade300,
                      title: 'see_detail_order'.tr,
                      textStyle: const TextStyle(color: Colors.blue),
                      onPressed: () {
                        Get.toNamed(Routes.DETAIL_ORDER);
                      },
                      styleButton: BaseButtonStyle.fill)
                  .marginOnly(right: 10),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildItemCancelOrder(int index) {
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.all(4),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.grey, width: 0.9),
          borderRadius: const BorderRadius.all(Radius.circular(20))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Mã giao dịch: 952$index',
              ),
              Container(
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                    color: Colors.orange.shade200,
                    borderRadius: BorderRadius.circular(12)),
                child: Text(
                  "cancel".tr,
                  style: TextStyle(
                      color: Colors.white, fontSize: ThemeProvider.fontSize12),
                ),
              )
            ],
          ).marginOnly(bottom: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 60,
                height: 60,
                child: CachedNetworkImage(
                  imageUrl:
                      'https://content.pancake.vn/1/s750x750/fwebp/f0/d8/39/5c/cf01e5b50bef662fda41584a977bc02cfa5a667c13b3215bef229b2f.jpg',
                ),
              ).marginOnly(right: 10),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('Thùng 100 Cái khẩu trang')
                      .marginOnly(bottom: 4.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'loại',
                        style: TextStyle(fontSize: ThemeProvider.fontSize12),
                      ),
                      Text(
                        'x số lượng',
                        style: TextStyle(fontSize: ThemeProvider.fontSize12),
                      )
                    ],
                  ).marginOnly(bottom: 4.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        'đ 50.000',
                        style: TextStyle(
                          fontSize: ThemeProvider.fontSize14,
                          decoration: TextDecoration.lineThrough,
                        ),
                      ).marginOnly(right: 10.w),
                      Text(
                        'đ 40.000',
                        style: TextStyle(
                            fontSize: ThemeProvider.fontSize14,
                            color: Colors.red),
                      )
                    ],
                  ),
                ],
              ))
            ],
          ),
          const Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('1 sản phẩm'),
              Row(
                children: [
                  const Text('Tổng thanh toán:'),
                  Text(
                    'đ 40.000',
                    style: TextStyle(
                        fontSize: ThemeProvider.fontSize16, color: Colors.red),
                  ),
                ],
              )
            ],
          ),
          const Divider(),
          Align(
              alignment: Alignment.centerRight,
              child: index % 2 == 0
                  ? Text('cod'.tr)
                  : Text(
                      'payment_ck'.tr,
                      style: const TextStyle(
                          color: Colors.green, fontStyle: FontStyle.italic),
                    )),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              BaseButton(
                      width: 180.w,
                      height: 60.h,
                      radius: 8,
                      backgroundColor: Colors.grey.shade300,
                      title: 'see_detail_order'.tr,
                      textStyle: const TextStyle(color: Colors.blue),
                      onPressed: () {
                        Get.toNamed(Routes.DETAIL_ORDER);
                      },
                      styleButton: BaseButtonStyle.fill)
                  .marginOnly(right: 10),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildItemShippingOrder(int index) {
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.all(4),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.grey, width: 0.9),
          borderRadius: const BorderRadius.all(Radius.circular(20))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Mã giao dịch: 952$index',
              ),
              Container(
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                    color: Colors.grey.shade200,
                    borderRadius: BorderRadius.circular(12)),
                child: Text(
                  'Hành trình đơn hàng',
                  style: TextStyle(
                      color: Colors.red, fontSize: ThemeProvider.fontSize12),
                ),
              )
            ],
          ).marginOnly(bottom: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 60,
                height: 60,
                child: CachedNetworkImage(
                  imageUrl:
                      'https://content.pancake.vn/1/s750x750/fwebp/f0/d8/39/5c/cf01e5b50bef662fda41584a977bc02cfa5a667c13b3215bef229b2f.jpg',
                ),
              ).marginOnly(right: 10),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('Thùng 100 Cái khẩu trang')
                      .marginOnly(bottom: 4.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'loại',
                        style: TextStyle(fontSize: ThemeProvider.fontSize12),
                      ),
                      Text(
                        'x số lượng',
                        style: TextStyle(fontSize: ThemeProvider.fontSize12),
                      )
                    ],
                  ).marginOnly(bottom: 4.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        'đ 50.000',
                        style: TextStyle(
                          fontSize: ThemeProvider.fontSize14,
                          decoration: TextDecoration.lineThrough,
                        ),
                      ).marginOnly(right: 10.w),
                      Text(
                        'đ 40.000',
                        style: TextStyle(
                            fontSize: ThemeProvider.fontSize14,
                            color: Colors.red),
                      )
                    ],
                  ),
                ],
              ))
            ],
          ),
          const Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('1 sản phẩm'),
              Row(
                children: [
                  const Text('Tổng thanh toán:'),
                  Text(
                    'đ 40.000',
                    style: TextStyle(
                        fontSize: ThemeProvider.fontSize16, color: Colors.red),
                  ),
                ],
              )
            ],
          ),
          const Divider(),
          Align(
              alignment: Alignment.centerRight,
              child: index % 2 == 0
                  ? Text('cod'.tr)
                  : Text(
                      'payment_ck'.tr,
                      style: const TextStyle(
                          color: Colors.green, fontStyle: FontStyle.italic),
                    )),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              BaseButton(
                      width: 180.w,
                      height: 60.h,
                      radius: 8,
                      backgroundColor: Colors.grey.shade300,
                      title: 'see_detail_order'.tr,
                      textStyle: const TextStyle(color: Colors.blue),
                      onPressed: () {
                        Get.toNamed(Routes.DETAIL_ORDER);
                      },
                      styleButton: BaseButtonStyle.fill)
                  .marginOnly(right: 10),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildItemWaittingOrder(int index) {
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.all(4),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.grey, width: 0.9),
          borderRadius: const BorderRadius.all(Radius.circular(20))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Mã giao dịch: 952$index',
              ),
              index % 2 == 0
                  ? const Text(
                      'Huỷ đơn',
                      style: TextStyle(color: Colors.red),
                    )
                  : const SizedBox()
            ],
          ).marginOnly(bottom: 5),
          Text(
            formatDate(DateTime.now()),
            style: TextStyle(fontSize: ThemeProvider.fontSize12),
          ).marginOnly(bottom: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 60,
                height: 60,
                child: CachedNetworkImage(
                  imageUrl:
                      'https://content.pancake.vn/1/s750x750/fwebp/f0/d8/39/5c/cf01e5b50bef662fda41584a977bc02cfa5a667c13b3215bef229b2f.jpg',
                ),
              ).marginOnly(right: 10),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('Thùng 100 Cái khẩu trang')
                      .marginOnly(bottom: 4.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'loại',
                        style: TextStyle(fontSize: ThemeProvider.fontSize12),
                      ),
                      Text(
                        'x số lượng',
                        style: TextStyle(fontSize: ThemeProvider.fontSize12),
                      )
                    ],
                  ).marginOnly(bottom: 4.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        'đ 50.000',
                        style: TextStyle(
                          fontSize: ThemeProvider.fontSize14,
                          decoration: TextDecoration.lineThrough,
                        ),
                      ).marginOnly(right: 10.w),
                      Text(
                        'đ 40.000',
                        style: TextStyle(
                            fontSize: ThemeProvider.fontSize14,
                            color: Colors.red),
                      )
                    ],
                  ),
                ],
              ))
            ],
          ),
          const Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('1 sản phẩm'),
              Row(
                children: [
                  const Text('Tổng thanh toán:'),
                  Text(
                    'đ 40.000',
                    style: TextStyle(
                        fontSize: ThemeProvider.fontSize16, color: Colors.red),
                  ),
                ],
              )
            ],
          ),
          const Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              BaseButton(
                      width: 180.w,
                      height: 60.h,
                      radius: 8,
                      backgroundColor: Colors.grey.shade300,
                      title: 'see_detail_order'.tr,
                      textStyle: const TextStyle(color: Colors.blue),
                      onPressed: () {
                        Get.toNamed(Routes.DETAIL_ORDER);
                      },
                      styleButton: BaseButtonStyle.fill)
                  .marginOnly(right: 10),
              index % 2 == 0
                  ? BaseButton(
                      width: 180.w,
                      height: 60.h,
                      radius: 8,
                      backgroundColor: Colors.grey.shade400,
                      title: 'wait_approve'.tr,
                      textStyle: const TextStyle(color: Colors.white),
                      onPressed: () {},
                      styleButton: BaseButtonStyle.fill)
                  : BaseButton(
                      width: 180.w,
                      height: 60.h,
                      radius: 8,
                      backgroundColor: Colors.blue.shade700,
                      title: '${'confirm'.tr}\nwaiting_shipping',
                      textStyle: const TextStyle(color: Colors.white),
                      onPressed: () {},
                      styleButton: BaseButtonStyle.fill)
            ],
          ),
        ],
      ),
    );
  }
}

String formatDate(DateTime date) {
  final timeFormat = DateFormat.Hm(); // Format for hours and minutes
  final dateFormat =
      DateFormat('dd/MM/yyyy'); // Format for day, date, and month

  final timeString = timeFormat.format(date);
  final dateString = dateFormat.format(date);

  // Assuming your day names are in Vietnamese, replace it accordingly
  final dayOfWeek = _getVietnameseDayName(date.weekday);

  return '$timeString $dayOfWeek, $dateString';
}

String _getVietnameseDayName(int dayOfWeek) {
  switch (dayOfWeek) {
    case 1:
      return 'Thứ 2';
    case 2:
      return 'Thứ 3';
    case 3:
      return 'Thứ 4';
    case 4:
      return 'Thứ 5';
    case 5:
      return 'Thứ 6';
    case 6:
      return 'Thứ 7';
    case 7:
      return 'Chủ Nhật';
    default:
      return '';
  }
}
