import 'package:get/get.dart';

class ListOrderController extends GetxController {
  final name = ''.obs;
  final index = 0.obs;
  @override
  void onInit() {
    _bindData();
    super.onInit();
  }

  void _bindData() {
    var index = Get.arguments['index'] as int;
    this.index.value = index;
    switch (index) {
      case 0:
        name.value = 'wait_approve'.tr;
        break;
      case 1:
        name.value = 'shipping'.tr;
        break;
      case 2:
        name.value = 'done'.tr;
        break;
      case 3:
        name.value = 'cancel'.tr;
        break;
      default:
        break;
    }
  }
}
