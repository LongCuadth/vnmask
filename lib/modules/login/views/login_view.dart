import 'package:app/base/widgets/base_button.dart';
import 'package:app/base/widgets/base_textfield.dart';
import 'package:app/gen/assets.gen.dart';
import 'package:app/routes/app_pages.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeProvider.colorWhite,
      body: SafeArea(
          child: Stack(
        children: [
          Positioned.fill(
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              physics: const BouncingScrollPhysics(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 30.h, width: double.maxFinite),
                  Assets.image.logo
                      .image(width: 100.w)
                      .marginOnly(bottom: 150.h),
                  BaseTextField(
                          textEditingController: controller.phoneController,
                          hintText: 'phone'.tr)
                      .marginSymmetric(horizontal: 50.w, vertical: 10.h),
                  BaseTextField(
                          isPassword: true,
                          textEditingController: controller.passwordController,
                          hintText: 'password'.tr)
                      .marginSymmetric(horizontal: 50.w, vertical: 10.h),
                  BaseButton(
                          title: 'forgot_pass'.tr,
                          onPressed: () {
                            Get.toNamed(Routes.FORGOT_PASS);
                          },
                          textStyle: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: ThemeProvider.fontSize12 + 1,
                              color: ThemeProvider.colorPrimary),
                          styleButton: BaseButtonStyle.textOnly)
                      .marginOnly(bottom: 20.h, top: 5.h),
                  BaseButton(
                          title: 'login'.tr,
                          onPressed: () {
                            Get.offAllNamed(Routes.MAIN_VIEW);
                          },
                          radius: 20,
                          textStyle: TextStyle(
                              fontSize: ThemeProvider.fontSize18,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                          styleButton: BaseButtonStyle.fill)
                      .marginSymmetric(horizontal: 50.w, vertical: 30.h)
                ],
              ),
            ),
          ),
          Positioned(
              bottom: 30,
              right: 0,
              left: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RichText(
                    text: TextSpan(
                        text: 'not_have_account'.tr,
                        style: TextStyle(
                            color: ThemeProvider.colorTextBlack,
                            fontSize: ThemeProvider.fontSize12),
                        children: [
                          TextSpan(
                            text: ' VN',
                            style: TextStyle(
                                color: Colors.red,
                                fontWeight: FontWeight.w500,
                                fontSize: ThemeProvider.fontSize12),
                          ),
                          TextSpan(
                            text: 'MASK',
                            style: TextStyle(
                                color: const Color(0xff3171b9),
                                fontWeight: FontWeight.w500,
                                fontSize: ThemeProvider.fontSize12),
                          ),
                          TextSpan(
                            text: '?',
                            style: TextStyle(
                                color: ThemeProvider.colorTextBlack,
                                fontSize: ThemeProvider.fontSize12),
                          )
                        ]),
                  ).marginOnly(right: 8.w),
                  GestureDetector(
                    onTap: () {
                      Get.toNamed(Routes.REGISTER);
                    },
                    child: Text(
                      'register_now'.tr,
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          color: Colors.red,
                          fontSize: ThemeProvider.fontSize12),
                    ),
                  )
                ],
              ))
        ],
      )),
    );
  }
}
