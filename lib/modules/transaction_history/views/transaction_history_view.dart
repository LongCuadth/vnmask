import 'package:app/modules/home/widgets/item_select_type.dart';
import 'package:app/modules/notification/controllers/notification_controller.dart';
import 'package:app/routes/app_pages.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../controllers/transaction_history_controller.dart';

class TransactionHistoryView extends GetView<TransactionHistoryController> {
  const TransactionHistoryView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: ThemeProvider.colorSecondary,
          elevation: 0,
          title: Container(
            padding: const EdgeInsets.all(4),
            width: Get.width / 2,
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(16))),
            child: Row(
              children: [
                const Icon(Icons.search, color: Colors.black)
                    .marginOnly(right: 12),
                Text(
                  'find_id_transaction'.tr,
                  style: TextStyle(
                      color: Colors.black, fontSize: ThemeProvider.fontSize12),
                )
              ],
            ),
          ),
          actions: [const Icon(Icons.calendar_month).marginOnly(right: 10)],
        ),
        body: Column(
          children: [
            _buildHeader(),
            Expanded(
                child: ListView.builder(
                    itemCount: 1,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: const EdgeInsets.all(20),
                        color: Colors.white,
                        child:
                            const Center(child: Text('Chưa có giao dịch nào')),
                      );
                    }))
          ],
        ));
  }

  Widget _buildHeader() {
    return Container(
      width: Get.width,
      padding: const EdgeInsets.only(bottom: 20),
      decoration: BoxDecoration(
          color: ThemeProvider.colorSecondary,
          borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(50),
              bottomRight: Radius.circular(50))),
      child: Column(
        children: [
          Container(
            width: double.maxFinite,
            margin: const EdgeInsets.symmetric(
              horizontal: 16,
              vertical: 8,
            ),
            padding: const EdgeInsets.all(8),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(16),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'my_order'.tr,
                  style: TextStyle(
                      fontSize: ThemeProvider.fontSize12,
                      fontStyle: FontStyle.italic),
                ),
                _buildStatusOrder()
              ],
            ),
          ),
          _buildTypeOrder()
        ],
      ),
    );
  }

  Widget _buildStatusOrder() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(
          onTap: () {
            Get.toNamed(Routes.LIST_ORDER, arguments: {'index': 0});
          },
          child: ItemStatusType(
            icon: const Icon(
              Icons.shopping_cart_outlined,
              color: Colors.amber,
            ),
            title: 'wait_approve'.tr,
          ),
        ),
        GestureDetector(
          onTap: () {
            Get.toNamed(Routes.LIST_ORDER, arguments: {'index': 1});
          },
          child: ItemStatusType(
            icon: const Icon(
              Icons.local_shipping_outlined,
              color: Colors.amber,
            ),
            title: 'shipping'.tr,
          ),
        ),
        GestureDetector(
          onTap: () {
            Get.toNamed(Routes.LIST_ORDER, arguments: {'index': 2});
          },
          child: ItemStatusType(
            icon: const Icon(
              Icons.domain_verification_sharp,
              color: Colors.amber,
            ),
            title: 'done'.tr,
          ),
        ),
        GestureDetector(
          onTap: () {
            Get.toNamed(Routes.LIST_ORDER, arguments: {'index': 3});
          },
          child: ItemStatusType(
            icon: const Icon(
              Icons.delete_forever_outlined,
              color: Colors.amber,
            ),
            title: 'cancel'.tr,
          ),
        ),
      ],
    );
  }

  Widget _buildTypeOrder() {
    return Obx(
      () => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () {
              controller.selectType.value = NotiType.all;
            },
            child: ItemSelectTransactionType(
              // width: 120,
              title: 'all'.tr,
              isSelected: controller.selectType.value == NotiType.all,
            ),
          ).marginOnly(right: 10.w),
          GestureDetector(
            onTap: () {
              controller.selectType.value = NotiType.shopping;
            },
            child: ItemSelectTransactionType(
                // width: 120,
                title: 'shopping'.tr,
                isSelected: controller.selectType.value == NotiType.shopping),
          ).marginOnly(right: 10.w),
          GestureDetector(
            onTap: () {
              controller.selectType.value = NotiType.phoneRecharge;
            },
            child: ItemSelectTransactionType(
                // width: 120,
                title: 'phone_recharge'.tr,
                isSelected:
                    controller.selectType.value == NotiType.phoneRecharge),
          ).marginOnly(right: 10.w),
          GestureDetector(
            onTap: () {
              controller.selectType.value = NotiType.addMoney;
            },
            child: ItemSelectTransactionType(
                // width: 120,
                title: 'add_money'.tr,
                isSelected: controller.selectType.value == NotiType.addMoney),
          ),
        ],
      ),
    );
  }
}
