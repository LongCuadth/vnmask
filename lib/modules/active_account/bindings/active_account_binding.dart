import 'package:get/get.dart';

import '../controllers/active_account_controller.dart';

class ActiveAccountBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ActiveAccountController>(
      () => ActiveAccountController(),
    );
  }
}
