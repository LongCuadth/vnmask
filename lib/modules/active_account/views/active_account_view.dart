import 'package:app/base/widgets/base_button.dart';
import 'package:app/base/widgets/base_textfield.dart';
import 'package:app/gen/assets.gen.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/active_account_controller.dart';

class ActiveAccountView extends GetView<ActiveAccountController> {
  const ActiveAccountView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeProvider.colorWhite,
      body: SafeArea(
          child: Stack(
        children: [
          Positioned.fill(
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              physics: const BouncingScrollPhysics(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 30.h, width: double.maxFinite),
                  Assets.image.logo
                      .image(width: 100.w)
                      .marginOnly(bottom: 20.h),
                  Text(
                    'active_account'.tr,
                    style: TextStyle(
                        fontSize: ThemeProvider.fontSize18,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ).marginOnly(bottom: 200.h),
                  BaseTextField(
                          inputType: TextInputType.number,
                          textEditingController: controller.otpController,
                          hintText: 'otp'.tr)
                      .marginSymmetric(horizontal: 50.w)
                      .marginOnly(bottom: 20.h),
                  RichText(
                      text: TextSpan(
                          text: 'otp_send_mail'.tr,
                          style: TextStyle(
                              color: ThemeProvider.colorTextBlack,
                              fontSize: ThemeProvider.fontSize14),
                          children: [
                        TextSpan(
                          text: ' ?',
                          style: TextStyle(
                              color: ThemeProvider.colorTextBlack,
                              fontWeight: FontWeight.w700,
                              fontSize: ThemeProvider.fontSize16),
                        )
                      ])).marginOnly(bottom: 180.h),
                  SizedBox(
                    width: 200.w,
                    child: BaseButton(
                        height: 30,
                        title: 'confirm'.tr,
                        onPressed: () {
                          Get.back();
                          Get.back();
                        },
                        radius: 8,
                        textStyle: TextStyle(
                            fontSize: ThemeProvider.fontSize18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                        styleButton: BaseButtonStyle.fill),
                  )
                ],
              ),
            ),
          ),
          Positioned(
              left: 10,
              child: InkWell(
                onTap: () {
                  Get.back();
                },
                child: const SizedBox(
                  child: Icon(
                    Icons.keyboard_double_arrow_left,
                    size: 34,
                    color: Colors.black,
                  ),
                ),
              ))
        ],
      )),
    );
  }
}
