import 'package:get/get.dart';

import '../controllers/money_wallet_controller.dart';

class MoneyWalletBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MoneyWalletController>(
      () => MoneyWalletController(),
    );
  }
}
