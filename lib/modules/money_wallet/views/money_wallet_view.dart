import 'package:app/gen/assets.gen.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/money_wallet_controller.dart';

class MoneyWalletView extends GetView<MoneyWalletController> {
  const MoneyWalletView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          toolbarHeight: 0,
          elevation: 0,
        ),
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _buildHeader().marginOnly(top: 5, bottom: 20),
              Row(
                children: [
                  Assets.image.ck.image(width: 40).marginOnly(right: 20.w),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'free_ck'.tr,
                        style: TextStyle(fontSize: ThemeProvider.fontSize14),
                      ),
                      Text(
                        'free_payment'.tr,
                        style: TextStyle(
                            fontSize: ThemeProvider.fontSize14 - 1,
                            fontStyle: FontStyle.italic),
                      )
                    ],
                  )
                ],
              ).marginSymmetric(horizontal: 16.w).marginOnly(bottom: 20.h),
              Text(
                'info_ck'.tr,
                style: TextStyle(
                    fontSize: ThemeProvider.fontSize14,
                    fontWeight: FontWeight.w600),
              ).marginSymmetric(horizontal: 16.w).marginOnly(bottom: 10.h),
              Text(
                'ck_24h'.tr,
                style: TextStyle(
                    fontSize: ThemeProvider.fontSize14,
                    fontWeight: FontWeight.w600),
              ).marginSymmetric(horizontal: 16.w).marginOnly(bottom: 10),
              Container(
                width: Get.width,
                padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 10.h),
                decoration: BoxDecoration(
                    color: Colors.grey.shade200,
                    borderRadius: BorderRadius.circular(16)),
                child: Column(
                  children: [
                    Assets.image.vcb.image(height: 30).marginOnly(bottom: 10.h),
                    const Text('Ngân hàng TMCP Ngoại thương Việt Nam')
                        .marginOnly(bottom: 20.h),
                    Row(
                      children: [
                        Text('Tên tài khoản nhận:').marginOnly(right: 50.w),
                        Text('NGUYỄN VĂN TIẾN'),
                      ],
                    ).marginOnly(bottom: 10.h),
                    Row(
                      children: [
                        Text('Số tài khoản:').marginOnly(right: 50.w),
                        Text('0451000292452').marginOnly(right: 10.w),
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 8.w, vertical: 4.h),
                          decoration: BoxDecoration(
                              border: Border.all(width: 0.5),
                              color: Colors.yellow.shade100,
                              borderRadius: BorderRadius.circular(16)),
                          child: Text('Copy'),
                        )
                      ],
                    ).marginOnly(bottom: 10.h),
                    Row(
                      children: [
                        Text('Nội dung chuyển khoản:').marginOnly(right: 30.w),
                        Text('0398973922').marginOnly(right: 10.w),
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 8.w, vertical: 4.h),
                          decoration: BoxDecoration(
                              border: Border.all(width: 0.5),
                              color: Colors.yellow.shade100,
                              borderRadius: BorderRadius.circular(16)),
                          child: Text('Copy'),
                        )
                      ],
                    ),
                  ],
                ),
              ).marginOnly(bottom: 20.h),
              RichText(
                      text: TextSpan(
                          text: 'note'.tr,
                          style: TextStyle(
                              color: Colors.red,
                              fontSize: ThemeProvider.fontSize14),
                          children: [
                    TextSpan(
                      text: 'note_message'.tr,
                      style: TextStyle(
                          color: ThemeProvider.colorTextBlack,
                          fontSize: ThemeProvider.fontSize14),
                    )
                  ]))
                  .marginSymmetric(horizontal: 16.w)
                  .marginOnly(bottom: 10.h),
              Text(
                'note_ck'.tr,
                style: TextStyle(
                    fontSize: ThemeProvider.fontSize14 - 1,
                    fontStyle: FontStyle.italic),
              ).marginSymmetric(horizontal: 16.w).marginOnly(bottom: 30.h),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      Assets.image.zalo
                          .image(height: 25)
                          .marginOnly(right: 10.w),
                      Text('zalo_support'.tr),
                    ],
                  ).marginOnly(bottom: 16.h),
                  Row(
                    children: [
                      Assets.image.messenger
                          .image(height: 25)
                          .marginOnly(right: 10.w),
                      Text('messenger_support'.tr),
                    ],
                  ),
                ],
              ).marginSymmetric(horizontal: Get.width / 4),
            ],
          ),
        ));
  }

  Widget _buildHeader() {
    return Stack(children: [
      SizedBox(
        width: double.infinity,
        height: 30,
        child: Center(
            child: Text(
          'add_money_wallet'.tr,
          style: TextStyle(
              color: Colors.black,
              fontSize: ThemeProvider.fontSize18,
              fontWeight: FontWeight.w600),
        )),
      ),
      Align(
        alignment: Alignment.centerLeft,
        child: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
            child: const Icon(
              Icons.arrow_back_ios,
            ),
          ),
        ),
      )
    ]);
  }
}
