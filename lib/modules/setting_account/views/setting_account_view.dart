import 'package:app/base/widgets/base_button.dart';
import 'package:app/base/widgets/base_textfield.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/setting_account_controller.dart';

class SettingAccountView extends GetView<SettingAccountController> {
  const SettingAccountView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          toolbarHeight: 0,
          elevation: 0,
        ),
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(children: [
                SizedBox(
                  width: double.infinity,
                  height: 30,
                  child: Center(
                      child: Text(
                    'setting_account'.tr,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: ThemeProvider.fontSize18,
                        fontWeight: FontWeight.w600),
                  )),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: GestureDetector(
                    onTap: () {
                      Get.back();
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8, vertical: 4),
                      child: const Icon(
                        Icons.arrow_back_ios,
                      ),
                    ),
                  ),
                )
              ]).marginOnly(top: 5),
              SizedBox(height: 20.h),
              _buildTextField('name'.tr, controller.nameController),
              SizedBox(height: 5.h),
              SizedBox(height: 20.h),
              _buildTextField('gmail'.tr, controller.gmailController),
              SizedBox(height: 5.h),
              SizedBox(height: 20.h),
              _buildTextField('phone'.tr, controller.phoneController),
              SizedBox(height: 50.h),
              Center(
                child: Text(
                  'confirm_change'.tr,
                  style: TextStyle(
                      fontSize: ThemeProvider.fontSize14,
                      fontWeight: FontWeight.w600),
                ),
              ),
              Center(
                child: BaseButton(
                    width: 120,
                    height: 30,
                    backgroundColor:
                        ThemeProvider.colorPrimary.withOpacity(0.1),
                    title: 'send_get_otp'.tr,
                    radius: 8,
                    onPressed: () {},
                    textStyle: TextStyle(
                        color: ThemeProvider.colorPrimary,
                        fontSize: ThemeProvider.fontSize14,
                        fontWeight: FontWeight.bold),
                    styleButton: BaseButtonStyle.fill),
              ).marginOnly(bottom: 10.h, top: 10.h),
              Center(
                child: SizedBox(
                  width: 180.w,
                  height: 70.h,
                  child: BaseTextField(
                          inputType: TextInputType.number,
                          textEditingController: controller.otpController,
                          hintText: 'otp'.tr)
                      .marginOnly(bottom: 10.h),
                ),
              ),
              Center(
                child: SizedBox(
                  width: 180.w,
                  height: 70.h,
                  child: BaseTextField(
                          inputType: TextInputType.number,
                          textEditingController: controller.passController,
                          hintText: 'password'.tr)
                      .marginOnly(bottom: 10.h),
                ),
              ),
              const Spacer(),
              Center(
                child: BaseButton(
                    width: 300,
                    height: 35,
                    title: 'update'.tr,
                    radius: 8,
                    onPressed: () {},
                    textStyle: TextStyle(
                        color: ThemeProvider.colorWhite,
                        fontSize: ThemeProvider.fontSize16,
                        fontWeight: FontWeight.bold),
                    styleButton: BaseButtonStyle.fill),
              ),
              SizedBox(height: 50.h),
            ],
          ),
        ));
  }

  Widget _buildTextField(String title, TextEditingController controller) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
              fontSize: ThemeProvider.fontSize14, fontWeight: FontWeight.w600),
        ),
        Stack(
          alignment: Alignment.centerRight,
          children: [
            Container(
              margin: EdgeInsets.only(top: 10.h),
              decoration: BoxDecoration(
                  color: Colors.grey.shade200,
                  borderRadius: const BorderRadius.all(Radius.circular(8))),
              child: TextFormField(
                cursorColor: Colors.black,
                controller: controller,
                style: TextStyle(
                    fontSize: ThemeProvider.fontSize12,
                    fontWeight: FontWeight.w600),
                decoration: const InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  contentPadding:
                      EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
                ),
              ),
            ),
            const Icon(
              Icons.edit_document,
              color: Colors.black,
              size: 16,
            ).marginOnly(right: 16),
          ],
        )
      ],
    ).paddingSymmetric(horizontal: 16.w);
  }
}
