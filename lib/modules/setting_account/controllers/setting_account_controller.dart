import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingAccountController extends GetxController {
  var nameController = TextEditingController();
  var gmailController = TextEditingController();
  var phoneController = TextEditingController();
  var otpController = TextEditingController();
  var passController = TextEditingController();
}
