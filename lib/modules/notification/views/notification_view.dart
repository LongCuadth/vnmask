import 'package:app/modules/home/widgets/item_select_type.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/notification_controller.dart';

class NotificationView extends GetView<NotificationController> {
  const NotificationView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 1,
          toolbarHeight: 0,
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Column(
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: GestureDetector(
                        onTap: () {
                          Get.back();
                        },
                        child: const Icon(
                          Icons.arrow_back_ios,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    Text(
                      'notification'.tr,
                      style: TextStyle(
                          fontSize: ThemeProvider.fontSize18,
                          fontWeight: FontWeight.w700),
                    )
                  ],
                ).marginSymmetric(horizontal: 16, vertical: 15),
                SizedBox(
                  height: 35.h,
                  child: Obx(
                    () => ListView(
                      physics: const BouncingScrollPhysics(),
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children: [
                        GestureDetector(
                          onTap: () {
                            controller.notiType.value = NotiType.all;
                          },
                          child: ItemSelectType(
                            width: 120,
                            title: 'all'.tr,
                            isSelected:
                                controller.notiType.value == NotiType.all,
                          ),
                        ).marginOnly(right: 10.w),
                        GestureDetector(
                          onTap: () {
                            controller.notiType.value = NotiType.shopping;
                          },
                          child: ItemSelectType(
                              width: 120,
                              title: 'shopping'.tr,
                              isSelected: controller.notiType.value ==
                                  NotiType.shopping),
                        ).marginOnly(right: 10.w),
                        GestureDetector(
                          onTap: () {
                            controller.notiType.value = NotiType.phoneRecharge;
                          },
                          child: ItemSelectType(
                              width: 120,
                              title: 'phone_recharge'.tr,
                              isSelected: controller.notiType.value ==
                                  NotiType.phoneRecharge),
                        ).marginOnly(right: 10.w),
                        GestureDetector(
                          onTap: () {
                            controller.notiType.value = NotiType.addMoney;
                          },
                          child: ItemSelectType(
                              width: 120,
                              title: 'add_money'.tr,
                              isSelected: controller.notiType.value ==
                                  NotiType.addMoney),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
