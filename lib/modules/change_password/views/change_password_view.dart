import 'package:app/base/widgets/base_button.dart';
import 'package:app/base/widgets/base_textfield.dart';
import 'package:app/gen/assets.gen.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import '../controllers/change_password_controller.dart';

class ChangePasswordView extends GetView<ChangePasswordController> {
  const ChangePasswordView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeProvider.colorWhite,
      body: SafeArea(
          child: Stack(
        children: [
          Positioned.fill(
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              physics: const BouncingScrollPhysics(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 30.h, width: double.maxFinite),
                  Assets.image.logo
                      .image(width: 100.w)
                      .marginOnly(bottom: 20.h),
                  Text(
                    'change_password'.tr,
                    style: TextStyle(
                        fontSize: ThemeProvider.fontSize18,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ).marginOnly(bottom: 100.h),
                  BaseTextField(
                          isPassword: true,
                          textEditingController: controller.oldPasssController,
                          hintText: 'current_password'.tr)
                      .marginSymmetric(horizontal: 50.w)
                      .marginOnly(bottom: 20.h),
                  BaseTextField(
                          isPassword: true,
                          textEditingController: controller.newPasssController,
                          hintText: 'new_password'.tr)
                      .marginSymmetric(horizontal: 50.w)
                      .marginOnly(bottom: 20.h),
                  BaseTextField(
                          isPassword: true,
                          textEditingController:
                              controller.reNewPasssController,
                          hintText: 're_newpassword'.tr)
                      .marginSymmetric(horizontal: 50.w)
                      .marginOnly(bottom: 50.h),
                  SizedBox(
                    width: 200.w,
                    child: BaseButton(
                        height: 30,
                        title: 'confirm_save'.tr,
                        onPressed: () {
                          Get.back();
                        },
                        radius: 8,
                        textStyle: TextStyle(
                            fontSize: ThemeProvider.fontSize18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                        styleButton: BaseButtonStyle.fill),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
              left: 10,
              child: InkWell(
                onTap: () {
                  Get.back();
                },
                child: const SizedBox(
                  child: Icon(
                    Icons.keyboard_double_arrow_left,
                    size: 34,
                    color: Colors.black,
                  ),
                ),
              ))
        ],
      )),
    );
  }
}
