import 'package:app/gen/assets.gen.dart';
import 'package:app/modules/detail_order/widget/item_product_cart.dart';
import 'package:app/modules/list_order/views/list_order_view.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/detail_order_controller.dart';

class DetailOrderView extends GetView<DetailOrderController> {
  const DetailOrderView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 2,
          backgroundColor: Colors.white,
          title: Text(
            'detail_order'.tr,
            style: const TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              )),
          actions: [
            SizedBox(width: 35, height: 35, child: Assets.image.zalo.image())
                .marginOnly(right: 20.w),
          ],
        ),
        body: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      formatDate(DateTime.now()),
                      style: TextStyle(fontSize: ThemeProvider.fontSize12),
                    ),
                    Text(
                      ' Mã giao dịch: 9521',
                      style: TextStyle(fontSize: ThemeProvider.fontSize12),
                    ),
                  ],
                ),
                _buildListItem(),
                Divider(
                  thickness: 2,
                  color: Colors.grey.shade200,
                ),
                _buildInfoPayMent()
              ],
            )));
  }

  Widget _buildListItem() {
    return ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: 1,
        itemBuilder: (context, index) {
          return ItemProductCart(
            indexProduct: 1,
            onChangeIndex: (index) {},
          );
        });
  }

  Widget _buildInfoPayMent() {
    return Column(children: [
      Container(
        height: 40,
        decoration: BoxDecoration(
          color: Colors.grey.shade200,
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(80), topRight: Radius.circular(80)),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        margin: const EdgeInsets.symmetric(horizontal: 20),
        child: Row(children: [
          ClipRRect(
              borderRadius: BorderRadius.circular(80),
              child: Assets.image.logo.image()),
          RichText(
            text: TextSpan(
                text: ''.tr,
                style: TextStyle(
                    color: ThemeProvider.colorTextBlack,
                    fontSize: ThemeProvider.fontSize12),
                children: [
                  TextSpan(
                    text: ' VN',
                    style: TextStyle(
                        color: Colors.red,
                        fontWeight: FontWeight.w500,
                        fontSize: ThemeProvider.fontSize12),
                  ),
                  TextSpan(
                    text: 'MASK',
                    style: TextStyle(
                        color: const Color(0xff3171b9),
                        fontWeight: FontWeight.w500,
                        fontSize: ThemeProvider.fontSize12),
                  ),
                  TextSpan(
                    text: ' Voucher',
                    style: TextStyle(
                        color: ThemeProvider.colorTextBlack,
                        fontSize: ThemeProvider.fontSize12),
                  )
                ]),
          ),
          const Spacer(),
          Text(
            'select_voucher'.tr,
            style: TextStyle(
                color: Colors.blue, fontSize: ThemeProvider.fontSize12),
          )
        ]),
      ),
      _buildMoneyInfo(),
      _buildCustomerInfo()
    ]);
  }

  Widget _buildMoneyInfo() {
    return Container(
      margin: EdgeInsets.only(top: 4.h),
      width: double.maxFinite,
      padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 20.h),
      decoration: BoxDecoration(
        color: Colors.grey.shade200,
        borderRadius: const BorderRadius.all(Radius.circular(20)),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('total_product'.tr),
              const Text('302.000đ'),
            ],
          ).marginOnly(bottom: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('shipping_pay'.tr),
              Text('25.000đ'),
            ],
          ).marginOnly(bottom: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('total_money'.tr),
              Text(
                '327.000đ',
                style: TextStyle(
                    color: ThemeProvider.colorPrimary,
                    fontWeight: FontWeight.w700),
              ),
            ],
          ).marginOnly(bottom: 5),
          Align(
              alignment: Alignment.topLeft,
              child: Text(
                'select_method_payment'.tr,
                style: TextStyle(fontSize: ThemeProvider.fontSize12),
              )).marginOnly(bottom: 5),
          SizedBox(
            height: 20,
            child: Row(
              children: [
                Checkbox(
                    fillColor: MaterialStateProperty.resolveWith(
                        (_) => ThemeProvider.colorPrimary),
                    shape: const CircleBorder(),
                    value: true,
                    onChanged: (value) {}),
                Text('cod'.tr),
              ],
            ),
          ).marginOnly(bottom: 5),
          SizedBox(
            height: 20,
            child: Row(
              children: [
                Checkbox(
                    fillColor: MaterialStateProperty.resolveWith(
                        (_) => ThemeProvider.colorWhite),
                    shape: const CircleBorder(),
                    value: false,
                    onChanged: (value) {}),
                Text('pay_ck'.tr),
              ],
            ),
          ).marginOnly(bottom: 10.h),
          Text(
            'shipping_time'.tr,
            style: TextStyle(
                fontStyle: FontStyle.italic,
                fontSize: ThemeProvider.fontSize12),
          ).marginOnly(bottom: 5.h),
          Text(
            'check_product'.tr,
            style: TextStyle(
                fontStyle: FontStyle.italic,
                color: ThemeProvider.colorPrimary,
                fontSize: ThemeProvider.fontSize12),
          )
        ],
      ),
    );
  }

  Widget _buildCustomerInfo() {
    return Container(
      margin: EdgeInsets.only(top: 4.h),
      width: double.maxFinite,
      padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 20.h),
      decoration: BoxDecoration(
        color: Colors.grey.shade200,
        border: Border.all(color: ThemeProvider.colorPrimary),
        borderRadius: const BorderRadius.all(
          Radius.circular(20),
        ),
      ),
      child: Column(
        children: [
          Text(
            'info_shipping'.tr,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ).marginOnly(bottom: 10.h),
          Row(
            children: [
              Expanded(
                  flex: 3,
                  child: Container(
                    margin: EdgeInsets.only(right: 8.w),
                    padding: const EdgeInsets.all(8),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(12),
                      ),
                    ),
                    child: const Text('Nguyễn Văn Tiến'),
                  )),
              Expanded(
                  flex: 2,
                  child: Container(
                    padding: const EdgeInsets.all(8),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(12),
                      ),
                    ),
                    child: const Text('0398973922'),
                  ))
            ],
          ).marginOnly(bottom: 10.h),
          Container(
            width: double.maxFinite,
            margin: EdgeInsets.only(right: 8.w),
            padding: const EdgeInsets.all(8),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
            ),
            child: const Text(
                'Địa chỉ: Xã nga phượng, huyện nga sơn, tỉnh thanh hoá'),
          ).marginOnly(bottom: 10.h),
          Container(
            width: double.maxFinite,
            margin: EdgeInsets.only(right: 8.w),
            padding: const EdgeInsets.all(8),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
            ),
            child: const Text('xóm 5 báo văn'),
          )
        ],
      ),
    );
  }
}
