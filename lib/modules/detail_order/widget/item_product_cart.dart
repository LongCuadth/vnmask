import 'package:app/themes/themes_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cart_stepper/cart_stepper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class ItemProductCart extends StatelessWidget {
  final bool isTrader;
  final int indexProduct;
  final Function(int index) onChangeIndex;
  const ItemProductCart(
      {super.key,
      this.isTrader = false,
      required this.indexProduct,
      required this.onChangeIndex});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.all(8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const Icon(
                  Icons.delete_forever_outlined,
                  size: 18,
                ),
                Text(
                  'delete'.tr,
                  style: TextStyle(
                      color: ThemeProvider.colorPrimary,
                      fontSize: ThemeProvider.fontSize12),
                )
              ],
            ),
          ),
          const SizedBox(width: 5),
          Expanded(
            flex: 11,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                isTrader
                    ? Stack(
                        children: [
                          SizedBox(
                            width: 80,
                            height: 80,
                            child: ClipRRect(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20.r)),
                              child: CachedNetworkImage(
                                imageUrl:
                                    'https://content.pancake.vn/1/s750x750/fwebp/f0/d8/39/5c/cf01e5b50bef662fda41584a977bc02cfa5a667c13b3215bef229b2f.jpg',
                              ),
                            ),
                          ),
                          Positioned(
                            top: 10,
                            child: Container(
                                padding: const EdgeInsets.all(4),
                                decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                      topRight: Radius.elliptical(15, 15),
                                      bottomRight: Radius.elliptical(15, 15),
                                    ),
                                    color: ThemeProvider.colorPrimary),
                                child: Text(
                                  'trader'.tr,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: ThemeProvider.fontSize10),
                                )),
                          )
                        ],
                      ).marginOnly(right: 10)
                    : SizedBox(
                        width: 60,
                        height: 60,
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(20.r)),
                          child: CachedNetworkImage(
                            imageUrl:
                                'https://content.pancake.vn/1/s750x750/fwebp/f0/d8/39/5c/cf01e5b50bef662fda41584a977bc02cfa5a667c13b3215bef229b2f.jpg',
                          ),
                        ),
                      ).marginOnly(right: 10),
                Expanded(
                  child: Column(
                    children: [
                      const Text('[Hộp 20 cái] Khẩu trang N95 SHB')
                          .marginOnly(bottom: 10.h),
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 12, vertical: 2),
                        decoration: BoxDecoration(
                            color: Colors.grey.shade200,
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.r))),
                        width: double.maxFinite,
                        child: Text(
                          'màu trắng 100 cái',
                          style: TextStyle(
                              fontSize: ThemeProvider.fontSize12,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ).marginOnly(bottom: 20.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('302.000 đ',
                              style: TextStyle(
                                  color: Colors.red,
                                  fontSize: ThemeProvider.fontSize18,
                                  fontWeight: FontWeight.bold)),
                          Center(
                            child: CartStepperInt(
                              value: indexProduct,
                              size: 25,
                              stepper: 1,
                              elevation: 0,
                              style: CartStepperTheme.of(context).copyWith(
                                  activeForegroundColor: Colors.black,
                                  activeBackgroundColor: Colors.grey.shade200,
                                  textStyle: TextStyle(
                                      color: Colors.black,
                                      fontSize: ThemeProvider.fontSize14)),
                              didChangeCount: (count) {
                                onChangeIndex.call(count);
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
