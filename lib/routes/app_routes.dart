// ignore_for_file: constant_identifier_names

part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const SPLASH = _Paths.SPLASH;
  static const HOME = _Paths.HOME;
  static const ACCOUNT = _Paths.ACCOUNT;
  static const LOGIN = _Paths.LOGIN;
  static const REGISTER = _Paths.REGISTER;
  static const ACTIVE_ACCOUNT = _Paths.ACTIVE_ACCOUNT;
  static const FORGOT_PASS = _Paths.FORGOT_PASS;
  static const CHANGE_PASSWORD = _Paths.CHANGE_PASSWORD;
  static const MAIN_VIEW = _Paths.MAIN_VIEW;

  static const CART = _Paths.CART;
  static const TRANSACTION_HISTORY = _Paths.TRANSACTION_HISTORY;
  static const PHONE_RECHARGE = _Paths.PHONE_RECHARGE;
  static const NOTIFICATION = _Paths.NOTIFICATION;
  static const SETTING_ACCOUNT = _Paths.SETTING_ACCOUNT;
  static const DETAIL_PRODUCT = _Paths.DETAIL_PRODUCT;
  static const VOUCHER_MANAGER = _Paths.VOUCHER_MANAGER;
  static const SHARE_APP = _Paths.SHARE_APP;
  static const MONEY_WALLET = _Paths.MONEY_WALLET;
  static const LIST_ORDER = _Paths.LIST_ORDER;
  static const DETAIL_ORDER = _Paths.DETAIL_ORDER;
}

abstract class _Paths {
  _Paths._();
  static const SPLASH = '/splash';
  static const HOME = '/home';
  static const ACCOUNT = '/account';
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const ACTIVE_ACCOUNT = '/active-account';
  static const FORGOT_PASS = '/forgot-pass';
  static const CHANGE_PASSWORD = '/change-password';
  static const MAIN_VIEW = '/main-view';
  static const CART = '/cart';
  static const TRANSACTION_HISTORY = '/transaction-history';
  static const PHONE_RECHARGE = '/phone-recharge';
  static const NOTIFICATION = '/notification';
  static const SETTING_ACCOUNT = '/setting-account';
  static const DETAIL_PRODUCT = '/detail-product';
  static const VOUCHER_MANAGER = '/voucher-manager';
  static const SHARE_APP = '/share-app';
  static const MONEY_WALLET = '/money-wallet';
  static const LIST_ORDER = '/list-order';
  static const DETAIL_ORDER = '/detail-order';
}
