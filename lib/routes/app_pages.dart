import 'package:get/get.dart';

import '../../modules/splash/bindings/splash_binding.dart';
import '../../modules/splash/views/splash_view.dart';
import '../modules/account/views/account_view.dart';
import '../modules/active_account/bindings/active_account_binding.dart';
import '../modules/active_account/views/active_account_view.dart';
import '../modules/cart/views/cart_view.dart';
import '../modules/change_password/bindings/change_password_binding.dart';
import '../modules/change_password/views/change_password_view.dart';
import '../modules/detail_order/bindings/detail_order_binding.dart';
import '../modules/detail_order/views/detail_order_view.dart';
import '../modules/detail_product/bindings/detail_product_binding.dart';
import '../modules/detail_product/views/detail_product_view.dart';
import '../modules/forgot_pass/bindings/forgot_pass_binding.dart';
import '../modules/forgot_pass/views/forgot_pass_view.dart';
import '../modules/home/views/home_view.dart';
import '../modules/list_order/bindings/list_order_binding.dart';
import '../modules/list_order/views/list_order_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/main_view/bindings/main_view_binding.dart';
import '../modules/main_view/views/main_view.dart';
import '../modules/money_wallet/bindings/money_wallet_binding.dart';
import '../modules/money_wallet/views/money_wallet_view.dart';
import '../modules/notification/bindings/notification_binding.dart';
import '../modules/notification/views/notification_view.dart';
import '../modules/phone_recharge/views/phone_recharge_view.dart';
import '../modules/register/bindings/register_binding.dart';
import '../modules/register/views/register_view.dart';
import '../modules/setting_account/bindings/setting_account_binding.dart';
import '../modules/setting_account/views/setting_account_view.dart';
import '../modules/share_app/bindings/share_app_binding.dart';
import '../modules/share_app/views/share_app_view.dart';
import '../modules/transaction_history/views/transaction_history_view.dart';
import '../modules/voucher_manager/bindings/voucher_manager_binding.dart';
import '../modules/voucher_manager/views/voucher_manager_view.dart';

// ignore_for_file: constant_identifier_names

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(
      name: _Paths.SPLASH,
      page: () => const SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.REGISTER,
      page: () => const RegisterView(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: _Paths.ACTIVE_ACCOUNT,
      page: () => const ActiveAccountView(),
      binding: ActiveAccountBinding(),
    ),
    GetPage(
      name: _Paths.FORGOT_PASS,
      page: () => const ForgotPassView(),
      binding: ForgotPassBinding(),
    ),
    GetPage(
      name: _Paths.CHANGE_PASSWORD,
      page: () => const ChangePasswordView(),
      binding: ChangePasswordBinding(),
    ),
    GetPage(
      name: _Paths.MAIN_VIEW,
      page: () => const MainView(),
      binding: MainViewBinding(),
    ),
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
    ),
    GetPage(
      name: _Paths.CART,
      page: () => const CartView(),
    ),
    GetPage(
      name: _Paths.TRANSACTION_HISTORY,
      page: () => const TransactionHistoryView(),
    ),
    GetPage(
      name: _Paths.ACCOUNT,
      page: () => const AccountView(),
    ),
    GetPage(
      name: _Paths.PHONE_RECHARGE,
      page: () => const PhoneRechargeView(),
    ),
    GetPage(
      name: _Paths.NOTIFICATION,
      page: () => const NotificationView(),
      binding: NotificationBinding(),
    ),
    GetPage(
      name: _Paths.SETTING_ACCOUNT,
      page: () => const SettingAccountView(),
      binding: SettingAccountBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_PRODUCT,
      page: () => const DetailProductView(),
      binding: DetailProductBinding(),
    ),
    GetPage(
      name: _Paths.VOUCHER_MANAGER,
      page: () => const VoucherManagerView(),
      binding: VoucherManagerBinding(),
    ),
    GetPage(
      name: _Paths.SHARE_APP,
      page: () => const ShareAppView(),
      binding: ShareAppBinding(),
    ),
    GetPage(
      name: _Paths.MONEY_WALLET,
      page: () => const MoneyWalletView(),
      binding: MoneyWalletBinding(),
    ),
    GetPage(
      name: _Paths.LIST_ORDER,
      page: () => const ListOrderView(),
      binding: ListOrderBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_ORDER,
      page: () => const DetailOrderView(),
      binding: DetailOrderBinding(),
    ),
  ];
}
