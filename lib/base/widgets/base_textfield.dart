import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BaseTextField extends StatefulWidget {
  final TextEditingController textEditingController;
  final String hintText;
  final String? Function(String?)? validator;
  final bool isPassword;
  final bool autoFocus;
  final TextInputType inputType;
  final List<TextInputFormatter>? inputFormatters;

  const BaseTextField(
      {super.key,
      required this.textEditingController,
      required this.hintText,
      this.isPassword = false,
      this.autoFocus = false,
      this.inputType = TextInputType.text,
      this.validator,
      this.inputFormatters});

  @override
  State<BaseTextField> createState() => _BaseTextFieldState();
}

class _BaseTextFieldState extends State<BaseTextField> {
  bool passwordToggle = true;

  @override
  void initState() {
    passwordToggle = widget.isPassword;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        TextFormField(
          autofocus: widget.autoFocus,
          controller: widget.textEditingController,
          keyboardType: widget.inputType,
          validator: widget.validator,
          obscureText: passwordToggle,
          obscuringCharacter: '●',
          inputFormatters: widget.inputFormatters,
          textAlign: TextAlign.center,
          textAlignVertical: TextAlignVertical.center,
          style: const TextStyle(
              fontWeight: FontWeight.bold, color: ThemeProvider.colorBlack),
          decoration: InputDecoration(
            contentPadding: EdgeInsets.zero,
            labelText: widget.hintText,
            labelStyle: const TextStyle(
                fontWeight: FontWeight.w400,
                color: ThemeProvider.colorHintText),
            floatingLabelStyle: const TextStyle(
                fontWeight: FontWeight.w400,
                color: ThemeProvider.colorHintText),
            alignLabelWithHint: true,
            floatingLabelAlignment: FloatingLabelAlignment.center,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            // hintText: widget.hintText,
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: const BorderSide(
                color: ThemeProvider.borderButtonOutline,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: const BorderSide(
                color: ThemeProvider.borderButtonOutline,
              ),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: const BorderSide(
                color: ThemeProvider.borderButtonOutline,
              ),
            ),
            hintStyle: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: ThemeProvider.fontSize14,
                color: ThemeProvider.colorBlack),
            errorStyle: TextStyle(
                fontSize: ThemeProvider.fontSize12,
                color: ThemeProvider.colorErrorText),
          ),
        ),
        Positioned(
            right: 10,
            child: Visibility(
              visible: widget.isPassword,
              child: InkWell(
                onTap: () {
                  setState(() {
                    passwordToggle = !passwordToggle;
                  });
                },
                child: Icon(
                  passwordToggle ? Icons.visibility : Icons.visibility_off,
                  size: 16,
                ),
              ),
            ))
      ],
    );
  }
}
