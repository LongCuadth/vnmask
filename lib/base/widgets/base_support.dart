import 'package:app/gen/assets.gen.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class BaseSupport extends StatelessWidget {
  const BaseSupport({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          'support'.tr,
          style: TextStyle(
              decoration: TextDecoration.underline,
              color: Colors.red,
              fontSize: ThemeProvider.fontSize12),
        ).marginOnly(right: 12.w),
        Assets.image.zalo.image(height: 30)
      ],
    );
  }
}
