import 'package:intl/intl.dart';

String currencyFormat(int money) {
  return NumberFormat('#,###,### đ').format(money).replaceAll(',', '.');
}
