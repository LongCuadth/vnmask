import 'package:app/flavors.dart';

class NetworkConfig {
  static String get baseAPI => BuildConfig.baseDomain;
}

class ApiPath {
  // AUTH
  static String apiLogin = '/api/login';
  static String checkphone = '/api/auth/check-phone';

  // Category
  static String apiGetListCategory = '/api/category';
  static String apiCreateCategory = '/api/category/generate';

  // Service
  static String apiGetListServices = '/api/service';
  static String apiCreateService = '/api/service/generate';
}
